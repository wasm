const std = @import("std");
const builtin = @import("builtin");

/// Decode `bytes` as `T` at comptime.
pub fn decode(comptime bytes: []const u8, comptime T: type) T {
    comptime {
        var stream = std.io.fixedBufferStream(bytes);
        return readInt(stream.reader(), T) catch unreachable;
    }
}

/// Read a LEB128 encoded integer.
pub fn readInt(reader: anytype, comptime T: type) !T {
    comptime std.debug.assert(@bitSizeOf(T) % 8 == 0);
    const U = std.meta.Int(.unsigned, @bitSizeOf(T));
    // Attribution: "Decode signed integer" https://en.wikipedia.org/wiki/LEB128
    var result: T = 0;
    var byte: u8 = undefined;
    var shift: std.math.Log2Int(T) = 0;
    var last_byte: bool = undefined;
    while (true) {
        byte = reader.readByte() catch |err| switch (err) {
            error.EndOfStream => return error.Malformed,
            else => |e| return e,
        };
        const merge, const shift_overflow = @shlWithOverflow(@as(U, byte & 0x7f), shift);
        // This handles bit overflow for unsigned integers. Some architectures might have a
        // dedicated instruction for shift with overflow detection. That'll theoretically make the
        // overflow check easier for unsigned integers. Presumably, this reduces to an unchecked
        // shift for signed integers.
        if (@typeInfo(T).Int.signedness == .unsigned and shift_overflow != 0)
            return error.Malformed;
        result |= @bitCast(merge);
        // Increment the shift by 7, erroring if the shift would overflow the `Log2Int(T)`. This is
        // the check for encoding an integer with too many bytes.
        const new_shift, const add_shift_overflow = @addWithOverflow(shift, 7);
        last_byte = add_shift_overflow == 1;
        // The extension bit is not set thus there are no more bytes to be read.
        if (byte & 0x80 == 0)
            break;
        if (last_byte)
            return error.Malformed;
        shift = new_shift;
    }

    if (@typeInfo(T).Int.signedness == .signed) {
        // Determine the sign of the integer.
        if (last_byte) {
            const used_bits = @bitSizeOf(T) % 7;
            const unused_bits = @popCount(byte >> used_bits);
            if (unused_bits != (7 - used_bits) * @as(u8, @intFromBool(result < 0)))
                return error.Malformed;
        } else if (byte & 0x40 != 0) {
            shift += 7;
            result |= ~@as(T, 0) << shift;
        }
    }

    return result;
}

fn expectReadInt(comptime T: type, expected: anyerror!T, encoded: []const u8) !void {
    var stream = std.io.fixedBufferStream(encoded);
    const result = readInt(stream.reader(), T);
    if (expected) |exp_int| {
        const actual = try result;
        errdefer std.debug.print("Expected: {}, Actual: {}\n", .{
            std.fmt.fmtSliceHexLower(std.mem.asBytes(&exp_int)),
            std.fmt.fmtSliceHexLower(std.mem.asBytes(&actual)),
        });
        try std.testing.expectEqual(exp_int, actual);
    } else |exp_err| {
        try std.testing.expectError(exp_err, result);
    }
}

test readInt {
    try expectReadInt(i32, -123456, "\xc0\xbb\x78");
    try expectReadInt(i32, -64, "\x40");
    try expectReadInt(i32, -1, "\x7f");
    try expectReadInt(i32, -1, "\xff\xff\xff\xff\x7f");
    try expectReadInt(u32, std.math.maxInt(u32), "\xff\xff\xff\xff\x0f");
    try expectReadInt(u64, std.math.maxInt(u64), "\xff\xff\xff\xff\xff\xff\xff\xff\xff\x01");
    try expectReadInt(u32, error.Malformed, "\x83\x80\x80\x80\x80\x00");
    try expectReadInt(u32, error.Malformed, "\x80\x80\x80\x80\x80\x00");
    try expectReadInt(u32, error.Malformed, "\x82\x80\x80\x80\x70");
    try expectReadInt(u8, 3, "\x83\x00");
    try expectReadInt(i16, -2, "\xfe\x7f");
    try expectReadInt(i16, -2, "\xfe\xff\x7f");
    try expectReadInt(u8, error.Malformed, "\x8e\x10");
    try expectReadInt(i8, error.Malformed, "\x8e\x3e");
    try expectReadInt(i8, error.Malformed, "\xff\x7b");
    try expectReadInt(i32, error.Malformed, "\x80\x80\x80\x80\x70");
}
