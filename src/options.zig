const root = @import("root");
const wasm_options = if (@hasDecl(root, "wasm_options")) @field(root, "wasm_options") else struct {};

const TraceEvent = @import("./trace.zig").TraceEvent;

pub const trace = option(?fn (event: TraceEvent) void, "trace", null);

fn option(comptime T: type, comptime name: []const u8, comptime default: T) T {
    if (@hasDecl(wasm_options, name))
        return @field(wasm_options, name);
    return default;
}
