const std = @import("std");

const trace = @import("trace.zig");

const Code = @import("./module.zig").Code;
const ExportDescription = @import("./module.zig").ExportDescription;
const ExternalType = @import("./module.zig").ExternalType;
const Function = @import("./module.zig").Function;
const FunctionIndex = @import("./module.zig").FunctionIndex;
const FunctionType = @import("./module.zig").FunctionType;
const GlobalType = @import("./module.zig").GlobalType;
const Limits = @import("./module.zig").Limits;
const LocalIndex = @import("./module.zig").LocalIndex;
const MemoryType = @import("./module.zig").MemoryType;
const Module = @import("./module.zig").Module;
const ReferenceType = @import("./module.zig").ReferenceType;
const Table = @import("./module.zig").Table;
const ValueType = @import("./module.zig").ValueType;

pub const page_size = 65536;

/// The "world" for a WASM environment.
///
/// All mutable state which persists across invocations belongs to the `Store`.
///
/// The relationship with a `Module` may be summarized as:
///
/// - Many modules may be instantiated within a store.
/// - A store may share access to a module.
/// - Modules instantiated within a store must outlive that store.
///
/// There is no provided method for deallocating resources; initialize with an allocator which is
/// capable of cleaning up all allocations such as an arena or stack allocator.
pub const Store = struct {
    alloc: std.mem.Allocator,
    imports: ImportMap = .{},
    next_import_group: u32 = 0,
    // Segmented lists are used here because they provide ordering, O(1) append, and stable
    // pointers. Ordering is required to efficiently serialize the store when transmitting or
    // persisting to disk. O(1) append is nice to have. Stable pointers are required to give raw
    // pointers to instances which is theoretically more efficient than indices. There may be other
    // benefits of which I'm less certain such as cache locality though that seems more dubious
    // since iteration over store items shouldn't be within the hot path.
    modules: std.SegmentedList(Instance, 0) = .{},
    functions: std.SegmentedList(FunctionInstance, 0) = .{},
    tables: std.SegmentedList(TableInstance, 0) = .{},
    memories: std.SegmentedList(MemoryInstance, 0) = .{},
    globals: std.SegmentedList(GlobalInstance, 0) = .{},
    elements: std.SegmentedList(ElementInstance, 0) = .{},
    data: std.SegmentedList(DataInstance, 0) = .{},

    pub fn init(alloc: std.mem.Allocator) Store {
        return .{ .alloc = alloc };
    }

    pub fn deinit(self: *Store) void {
        // TODO: Evaluate whether an arena is better here. This should involve:
        // - Is it a use case to deallocate individual instances?
        // - Performance evaluation of creating/destroying elements within a store.
        // - Performance evaluation of creating/destroying the store itself.
        // A difficultly with an arena comes when attempting to grow memory or tables. Those should
        // not be done within an arena since the reallocation process would leave behind the
        // previous slice, and those allocations are rather large. However, managing mixed
        // allocations like that will be a large mental overhead.

        // Free imports.
        self.imports.deinit(self.alloc);
        // Free modules.
        var module_iter = self.modules.constIterator(0);
        while (module_iter.next()) |inst| {
            self.alloc.free(inst.functions);
            self.alloc.free(inst.tables);
            self.alloc.free(inst.memories);
            self.alloc.free(inst.globals);
            self.alloc.free(inst.elements);
            self.alloc.free(inst.data);
        }
        self.modules.deinit(self.alloc);
        // Free functions.
        self.functions.deinit(self.alloc);
        // Free tables.
        var table_iter = self.tables.constIterator(0);
        while (table_iter.next()) |inst|
            self.alloc.free(inst.entries);
        self.tables.deinit(self.alloc);
        // Free memories.
        var memory_iter = self.memories.constIterator(0);
        while (memory_iter.next()) |inst|
            self.alloc.free(inst.data);
        self.memories.deinit(self.alloc);
        // Free globals.
        self.globals.deinit(self.alloc);
        // Free elements.
        var element_iter = self.elements.constIterator(0);
        while (element_iter.next()) |inst|
            self.alloc.free(inst.entries);
        self.elements.deinit(self.alloc);
        // Free data. The `data` field within each instance does not belond to this structure.
        self.data.deinit(self.alloc);
    }

    pub fn drop(self: *Store, memory: anytype) void {
        self.alloc.free(memory);
    }

    pub const InstantiateError = std.mem.Allocator.Error || Trap || error{
        MissingImport,
        IncompatibleImport,
    };

    pub fn instantiate(
        self: *Store,
        imports: ImportGroup,
        module: *const Module,
        interpreter: *Interpreter,
    ) InstantiateError!*Instance {
        // Since all the allocated instances are stored by segmented lists within the store, the
        // capacity beyond the starting length of each list effectively acts as an arena for that
        // type of allocation. Cleaning up the allocations is as simple as resetting back to the
        // previous length. This will retain additional allocated capacity.
        const starting_modules_len = self.modules.len;
        const starting_functions_len = self.functions.len;
        const starting_tables_len = self.tables.len;
        const starting_memories_len = self.memories.len;
        const starting_globals_len = self.globals.len;
        const starting_elements_len = self.elements.len;
        const starting_data_len = self.data.len;
        errdefer {
            self.modules.shrink(starting_modules_len);
            self.functions.shrink(starting_functions_len);
            self.tables.shrink(starting_tables_len);
            self.memories.shrink(starting_memories_len);
            self.globals.shrink(starting_globals_len);
            self.elements.shrink(starting_elements_len);
            self.data.shrink(starting_data_len);
        }

        // Instantiate a module instance as dictated by the specification. This function loosely
        // follows the pseudo-code of the specification but should strictly follow the semantics.
        //
        // Steps 1 is assumed from validation during `Module.load`.
        //
        // Ref: WebAssembly Specification, Release 2.0 (Draft 2024-03-05), Section 4.5.4

        // Calculate the size of each of the arrays which may involve imports. These counts are
        // produced during validation/compilation to aid the allocations here.
        const funcs_len = module.imported_functions + module.funcs.len;
        const tables_len = module.imported_tables + module.tables.len;
        const memories_len = module.imported_memories + module.memories.len;
        const globals_len = module.imported_globals + module.globals.len;

        // Create the module instance.
        const instance: *Instance = try self.create(.modules);
        // Allocate all the arrays for the module instance.
        const functions = try self.alloc.alloc(*const FunctionInstance, funcs_len);
        errdefer self.alloc.free(functions);
        var tables_idx: usize = 0;
        const tables = try self.alloc.alloc(*TableInstance, tables_len);
        errdefer {
            const owned_tables_end = @max(tables_idx, module.imported_tables);
            const owned_tables = tables[module.imported_tables..owned_tables_end];
            for (owned_tables) |table|
                self.alloc.free(table.entries);
            self.alloc.free(tables);
        }
        var memories_idx: usize = 0;
        const memories = try self.alloc.alloc(*MemoryInstance, memories_len);
        errdefer {
            const owned_memories_end = @max(memories_idx, module.imported_memories);
            const owned_memories = memories[module.imported_memories..owned_memories_end];
            for (owned_memories) |memory|
                self.alloc.free(memory.data);
            self.alloc.free(memories);
        }
        const globals = try self.alloc.alloc(*GlobalInstance, globals_len);
        errdefer self.alloc.free(globals);
        var element_idx: usize = 0;
        const elements = try self.alloc.alloc(*ElementInstance, module.elements.len);
        errdefer {
            for (elements[0..element_idx]) |eleminst|
                self.alloc.free(eleminst.entries);
            self.alloc.free(elements);
        }
        const data = try self.alloc.alloc(*DataInstance, module.data.len);
        errdefer self.alloc.free(data);

        // These indices are used while applying imports.
        var funcs_idx: usize = 0;
        var globals_idx: usize = 0;

        // TODO: What specifically does step 2 want to be asserted?

        // Step 3: Verify the number of imports matches the number of declared imports.
        // TODO: Is this safe to remove this validation step? It seems like `imports` should be
        // permitted to have more imports than the module calls for.
        // if (module.imports.len != imports.count())
        //     return error.Fail;
        // Step 4: Apply each of the imports to the instance, validating the type.
        for (module.imports) |import| {
            const import_key = .{
                .group = imports,
                .module = import.module,
                .name = import.name,
            };
            const value = self.imports.get(import_key) orelse
                return error.MissingImport;
            if (std.meta.activeTag(import.desc) != std.meta.activeTag(value))
                return error.IncompatibleImport;
            switch (value) {
                .function => |func| {
                    const exp_type = module.types[import.desc.function.type_index];
                    if (!func.type.matches(exp_type))
                        return error.IncompatibleImport;
                    functions[funcs_idx] = func;
                    funcs_idx += 1;
                },
                .table => |table| {
                    if (!table.type.matches(import.desc.table))
                        return error.IncompatibleImport;
                    tables[tables_idx] = table;
                    tables_idx += 1;
                },
                .memory => |mem| {
                    if (!mem.limits.matches(import.desc.memory.limits))
                        return error.IncompatibleImport;
                    memories[memories_idx] = mem;
                    memories_idx += 1;
                },
                .global => |global| {
                    if (!global.type.matches(import.desc.global))
                        return error.IncompatibleImport;
                    globals[globals_idx] = global;
                    globals_idx += 1;
                },
            }
        }

        // Steps 5-10: Initialize globals and element segments. These steps are performed while
        // allocating the globals in step 11 (mentioned here to explain the numbering gap which
        // would otherwise exist). The specification draws upon execution semantics to initialize
        // within a stripped down auxiliary module instance which only has the imported globals and
        // the allocated function addresses. This is functionally the same as running initialization
        // within the final instance which has been partially initialized while constricting
        // global/function indexing. Here, we assume validation during `Module.load` handles the
        // restriction.

        // Step 11: Allocate all the components from `module` within the store. This single step
        // does a lot of work since it references out to allocation.
        //
        // Ref: WebAssembly Specification, Release 2.0 (Draft 2024-03-05), Section 4.5.3

        // Allocate each of the functions within the store.
        for (functions[funcs_idx..], module.funcs) |*slot, *func| {
            const funcinst = try self.create(.functions);
            funcinst.* = .{
                // Retains reference to data allocated within `module`.
                .type = &module.types[func.type.type_index],
                .impl = .{ .wasm = .{
                    .module = instance,
                    .code = func,
                } },
            };
            slot.* = funcinst;
        }

        // Allocate each of the tables within the store.
        for (tables[tables_idx..], module.tables) |*slot, table| {
            const tableinst = try self.create(.tables);
            // TODO: Define and enforce host defined limits of some sort.
            const table_entries = try self.alloc.alloc(Value, table.limits.min);
            errdefer self.alloc.free(table_entries);
            @memset(table_entries, switch (table.type) {
                .funcref => .{ .funcref = null },
                .externref => .{ .externref = null },
            });
            tableinst.* = .{
                .type = table,
                .entries = table_entries,
            };
            slot.* = tableinst;
            tables_idx += 1;
        }

        // Allocate each of the memories within the store.
        for (memories[memories_idx..], module.memories) |*slot, memory| {
            const memoryinst = try self.create(.memories);
            // TODO: Define and enforce host defined limits of some sort.
            const memory_size = @as(usize, memory.limits.min) * page_size;
            const bytes = try self.alloc.alloc(u8, memory_size);
            @memset(bytes, 0x00);
            memoryinst.* = .{
                .limits = memory.limits,
                .data = bytes,
            };
            slot.* = memoryinst;
            memories_idx += 1;
        }

        // Allocate each of the globals within the store.
        for (globals[globals_idx..], module.globals) |*slot, *global| {
            const globalinst = try self.create(.globals);
            globalinst.* = .{
                .type = &global.type,
                .value = switch (global.init) {
                    .import => |idx| globals[idx.global_index].value,
                    .funcref => |idx| .{
                        .funcref = if (idx) |i| functions[i.func_index] else null,
                    },
                    inline else => |payload, tag| @unionInit(Value, @tagName(tag), payload),
                },
            };
            slot.* = globalinst;
        }

        // Steps 12-13: Setup for explaining executing embed WASM instructions within an fake frame.
        // These instructions are rather encoded as Zig logic here.

        // Steps 14-15: Initialize tables from the element segments.
        for (module.elements) |segment| {
            const seginst = try self.create(.elements);
            // TODO: This unnecessarily allocates for active segments. The allocation can be
            // optimized away.
            const segment_entries = try self.alloc.alloc(Value, segment.init.len);
            errdefer self.alloc.free(segment_entries);
            for (segment_entries, segment.init) |*element, initializer| {
                element.* = switch (initializer) {
                    .import => |idx| globals[idx.global_index].value,
                    .funcref => |idx| .{
                        .funcref = if (idx) |i| functions[i.func_index] else null,
                    },
                    inline else => |payload, tag| @unionInit(Value, @tagName(tag), payload),
                };
            }
            seginst.* = .{
                .type = segment.type,
                .entries = segment_entries,
            };
            switch (segment.mode) {
                .active => {
                    const active = segment.mode.active;
                    const offset = switch (active.offset) {
                        .import => |idx| globals[idx.global_index].value,
                        .i32 => |i| Value{ .i32 = i },
                        else => unreachable,
                    }.i32;
                    const table = tables[active.table.table_index];
                    const end = offset + segment.init.len;
                    if (offset > table.entries.len)
                        return error.TrapOutOfBounds;
                    if (end > table.entries.len)
                        return error.TrapOutOfBounds;
                    @memcpy(table.entries[offset..end], seginst.entries);
                    // table.drop
                    self.drop(seginst.entries);
                    seginst.entries = &.{};
                },
                .declarative => {
                    self.drop(seginst.entries);
                    seginst.entries = &.{};
                },
                else => {},
            }
            elements[element_idx] = seginst;
            element_idx += 1;
        }

        // Step 16: Apply all active data segments and allocate each passive data segment within
        // the store.
        for (module.data, data) |datum, *slot| {
            const datainst = try self.create(.data);
            switch (datum) {
                .passive => |bytes| {
                    datainst.* = .{ .data = bytes };
                },
                // This branch covers step 16 of instantiation.
                .active => |active| {
                    // Assertion dictated by the specification.
                    std.debug.assert(active.memory.mem_index == 0);
                    const meminst = memories[active.memory.mem_index];
                    const offset = switch (active.offset) {
                        .import => |idx| globals[idx.global_index].value,
                        .i32 => |i| Value{ .i32 = i },
                        else => unreachable,
                    }.i32;
                    const end = offset + active.data.len;
                    if (offset > meminst.data.len)
                        return error.TrapOutOfBounds;
                    if (end > meminst.data.len)
                        return error.TrapOutOfBounds;
                    @memcpy(meminst.data[offset..end], active.data);
                    datainst.* = .{ .data = &.{} };
                },
            }
            slot.* = datainst;
        }

        instance.* = .{
            .module = module,
            // Retains reference to data allocated within `module`.
            .types = module.types,
            .functions = functions,
            .tables = tables,
            .memories = memories,
            .globals = globals,
            .elements = elements,
            .data = data,
        };

        // Steps 17-19: Run the start function to completion.
        if (module.start) |start| {
            const start_function = instance.functions[start.func_index];
            const results = try interpreter.call(self, start_function, &.{});
            std.debug.assert(results.len == 0);
        }

        return instance;
    }

    /// A trace point for allocating a new value within each of the fields.
    fn create(
        self: *Store,
        comptime tag: std.meta.FieldEnum(Store),
    ) !switch (tag) {
        .modules => *Instance,
        .functions => *FunctionInstance,
        .tables => *TableInstance,
        .memories => *MemoryInstance,
        .globals => *GlobalInstance,
        .elements => *ElementInstance,
        .data => *DataInstance,
        else => unreachable,
    } {
        return try @field(self.*, @tagName(tag)).addOne(self.alloc);
    }

    const ImportMap = std.HashMapUnmanaged(
        KeyPath,
        ExternalValue,
        HashContext,
        std.hash_map.default_max_load_percentage,
    );

    pub const ImportGroup = packed struct(u32) { id: u32 };

    pub fn importGroup(self: *Store) ImportGroup {
        const result = .{ .id = self.next_import_group };
        self.next_import_group += 1;
        return result;
    }

    const KeyPath = struct {
        group: ImportGroup,
        module: []const u8,
        name: []const u8,
    };

    const HashContext = struct {
        pub fn hash(_: HashContext, key: KeyPath) u64 {
            var hasher = std.hash.Wyhash.init(0);
            hasher.update(std.mem.asBytes(&key.group.id));
            hasher.update(std.mem.asBytes(&key.module.len));
            hasher.update(key.module);
            hasher.update(std.mem.asBytes(&key.name.len));
            hasher.update(key.name);
            return hasher.final();
        }

        pub fn eql(_: HashContext, lhs: KeyPath, rhs: KeyPath) bool {
            return lhs.group.id == rhs.group.id and
                std.mem.eql(u8, lhs.module, rhs.module) and
                std.mem.eql(u8, lhs.name, rhs.name);
        }
    };

    pub fn register(
        self: *Store,
        group: ImportGroup,
        name: []const u8,
        instance: *const Instance,
    ) LinkError!void {
        for (instance.module.exports) |exp| {
            const externval: ExternalValue = switch (exp.desc) {
                .function => |idx| .{ .function = instance.functions[idx.func_index] },
                .table => |idx| .{ .table = instance.tables[idx.table_index] },
                .memory => |idx| .{ .memory = instance.memories[idx.mem_index] },
                .global => |idx| .{ .global = instance.globals[idx.global_index] },
            };
            try self.link(group, name, exp.name, externval);
        }
    }

    pub fn createFunction(
        self: *Store,
        group: ImportGroup,
        module: []const u8,
        name: []const u8,
        typ: *const FunctionType,
        context: *anyopaque,
        func: *const HostFunction,
    ) LinkError!void {
        const starting_functions_len = self.functions.len;
        const slot = try self.create(.functions);
        errdefer self.functions.shrinkCapacity(self.alloc, starting_functions_len);
        slot.* = .{
            .type = typ,
            .impl = .{ .host = .{
                .context = context,
                .func = func,
            } },
        };
        try self.link(group, module, name, .{ .function = slot });
    }

    pub fn createTable(
        self: *Store,
        group: ImportGroup,
        module: []const u8,
        name: []const u8,
        typ: Table,
    ) LinkError!void {
        const starting_tables_len = self.tables.len;
        const slot = try self.create(.tables);
        errdefer self.tables.shrinkCapacity(self.alloc, starting_tables_len);
        const entries = try self.alloc.alloc(Value, typ.limits.min);
        errdefer self.alloc.free(entries);
        @memset(entries, switch (typ.type) {
            .funcref => .{ .funcref = null },
            .externref => .{ .externref = null },
        });
        slot.* = .{
            .type = typ,
            .entries = entries,
        };
        try self.link(group, module, name, .{ .table = slot });
    }

    pub fn createMemory(
        self: *Store,
        group: ImportGroup,
        module: []const u8,
        name: []const u8,
        limits: Limits,
    ) LinkError!void {
        const starting_memories_len = self.memories.len;
        const slot = try self.create(.memories);
        errdefer self.memories.shrinkCapacity(self.alloc, starting_memories_len);
        const data = try self.alloc.alloc(u8, limits.min * page_size);
        errdefer self.alloc.free(data);
        @memset(data, 0);
        slot.* = .{
            .limits = limits,
            .data = data,
        };
        try self.link(group, module, name, .{ .memory = slot });
    }

    pub fn createGlobal(
        self: *Store,
        group: ImportGroup,
        module: []const u8,
        name: []const u8,
        typ: *const GlobalType,
        default: ?Value,
    ) LinkError!void {
        const slot = try self.create(.globals);
        slot.* = .{
            .type = typ,
            .value = default orelse typ.type.default(),
        };
        try self.link(group, module, name, .{ .global = slot });
    }

    pub const LinkError = std.mem.Allocator.Error;

    const ExternalValue = union(ExternalType) {
        function: *const FunctionInstance,
        table: *TableInstance,
        memory: *MemoryInstance,
        global: *GlobalInstance,
    };

    fn link(
        self: *Store,
        group: ImportGroup,
        module: []const u8,
        name: []const u8,
        externval: ExternalValue,
    ) LinkError!void {
        const key = .{ .group = group, .module = module, .name = name };
        try self.imports.put(self.alloc, key, externval);
    }
};

/// A module instantiated within a store.
///
/// This provides a mapping between a module's indices to host addresses.
pub const Instance = struct {
    module: *const Module,
    types: []const FunctionType,
    functions: []const *const FunctionInstance,
    tables: []const *TableInstance,
    memories: []const *MemoryInstance,
    globals: []const *GlobalInstance,
    elements: []const *ElementInstance,
    data: []const *DataInstance,
};

pub const FunctionInstance = struct {
    type: *const FunctionType,
    impl: union(enum) {
        wasm: struct {
            module: *const Instance,
            code: *const Function,
        },
        host: struct {
            context: *anyopaque,
            func: *const HostFunction,
        },
    },
};

pub const TableInstance = struct {
    type: Table,
    entries: []Value,
};

pub const MemoryInstance = struct {
    limits: Limits,
    data: []u8,
};

pub const GlobalInstance = struct {
    type: *const GlobalType,
    value: Value,
};

pub const ElementInstance = struct {
    type: ReferenceType,
    entries: []const Value,
};

pub const DataInstance = struct {
    data: []const u8,
};

/// A single WASM value.
pub const Value = union {
    i32: u32,
    i64: u64,
    f32: f32,
    f64: f64,
    v128: [16]u8,
    externref: ?*anyopaque,
    funcref: ?*const FunctionInstance,
};

pub const Interpreter = struct {
    alloc: std.mem.Allocator,
    limits: struct {
        stack: usize = std.math.maxInt(u16),
    } = .{},
    stack: std.ArrayListUnmanaged(Entry) = .{},
    scratch: std.ArrayListUnmanaged(Value) = .{},
    activation_link: usize = undefined,
    activation: [*]const Code = undefined,
    instance: *const Instance = undefined,
    program_counter: usize = undefined,
    label_link: usize = undefined,

    pub fn init(alloc: std.mem.Allocator) Interpreter {
        return .{ .alloc = alloc };
    }

    pub fn deinit(self: *Interpreter) void {
        self.stack.deinit(self.alloc);
        self.scratch.deinit(self.alloc);
    }

    const GROW_ERROR_VALUE = Value{ .i32 = @bitCast(@as(i32, -1)) };

    pub fn call(
        self: *Interpreter,
        store: *Store,
        func: *const FunctionInstance,
        parameters: []const Value,
    ) EvalError![]const Value {
        std.debug.assert(self.stack.items.len == 0);
        self.scratch.clearRetainingCapacity();
        // When exiting cleanly, the stack should be empty. When exiting with an error, the stack
        // can be in any state but should be cleared for future use.
        defer std.debug.assert(self.stack.items.len == 0);
        errdefer self.stack.clearRetainingCapacity();

        // Assert the parameters match the existing stack values.
        std.debug.assert(func.type.parameters.len == parameters.len);

        // Push the initial function call frame.
        switch (func.impl) {
            .wasm => |impl| {
                // Allocate space for the function's stack.
                try self.allocFrame(impl.code);

                // Push the locals in reverse order.
                var local_iter = std.mem.reverseIterator(impl.code.locals[parameters.len..]);
                while (local_iter.next()) |local|
                    self.push(.local, local.default());
                var parameter_iter = std.mem.reverseIterator(parameters);
                while (parameter_iter.next()) |parameter|
                    self.push(.local, parameter);

                // Push the frame.
                self.save(.activation_link);
                self.save(.program_counter);
                self.save(.instance);
                self.save(.activation);
                self.activation_link = self.stack.items.len;
                self.activation = impl.code.body.ptr;
                self.instance = impl.module;
                self.program_counter = 1;
                self.save(.label_link);
                self.push(.label_continuation, impl.code.body.len - 1);
                self.label_link = self.stack.items.len;
            },
            .host => |impl| {
                // This should compile out in release builds.
                // TODO: Confirm this compiles out in release builds.
                self.assertStack(func.type.parameters);
                // Call the host function with a restricted view of `self`. The host
                // function should only be able to pop parameter values and push result
                // values.
                try impl.func(impl.context, @ptrCast(self));
                // This should compile out in release builds.
                // TODO: Confirm this compiles out in release builds.
                self.assertStack(func.type.results);
                std.mem.reverse(Value, self.scratch.items);
                return self.scratch.items;
            },
        }

        while (true) {
            switch (self.next(.opcode)) {
                .@"unreachable" => return error.TrapUnreachable,
                .nop => {},
                .block => {
                    const arity = self.next(.arity);
                    const block_size = self.next(.size);
                    const continuation = self.program_counter + block_size;

                    try self.saveScratch(arity);
                    self.save(.label_link);
                    self.push(.label_continuation, continuation);
                    self.label_link = self.stack.items.len;
                    self.pushScratch(.value);
                },
                .loop => {
                    const arity = self.next(.arity);

                    // MAGIC: subtract two to get the program counter for the loop instruction
                    // itself rather than the interior block of the loop.
                    const continuation = self.program_counter - 2;
                    try self.saveScratch(arity);
                    self.save(.label_link);
                    self.push(.label_continuation, continuation);
                    self.label_link = self.stack.items.len;
                    self.pushScratch(.value);
                },
                .@"if" => {
                    const arity = self.next(.arity);
                    const block_size = self.next(.size);
                    const continuation = self.program_counter + block_size;
                    const if_block_size = self.next(.size);
                    const otherwise = self.program_counter + if_block_size - 1;

                    const condition = self.pop(.value).i32;
                    try self.saveScratch(arity);
                    self.save(.label_link);
                    self.push(.label_continuation, continuation);
                    self.label_link = self.stack.items.len;
                    self.pushScratch(.value);

                    if (condition == 0)
                        self.program_counter = otherwise;
                },
                .@"else" => self.program_counter += self.next(.size),

                .end => {
                    try self.saveScratch(self.stack.items.len - self.label_link);
                    _ = self.pop(.label_continuation);
                    self.restore(.label_link);
                    if (self.activation_link == self.stack.items.len) {
                        self.completeFrame();
                        if (self.stack.items.len == 0)
                            break;
                    }
                    self.pushScratch(.value);
                },

                .br => {
                    const arity = self.next(.arity);
                    const label = self.next(.label).label_index;

                    try self.saveScratch(arity);
                    self.gotoLabel(label);
                    self.pushScratch(.value);
                },
                .br_if => {
                    const arity = self.next(.arity);
                    const label = self.next(.label).label_index;

                    if (self.pop(.value).i32 != 0) {
                        try self.saveScratch(arity);
                        self.gotoLabel(label);
                        self.pushScratch(.value);
                    }
                },
                .br_table => {
                    const arity = self.next(.arity);
                    const len = self.next(.size);

                    const break_idx = self.pop(.value).i32;
                    const label = for (0..len) |idx| {
                        const label = self.next(.label).label_index;
                        if (break_idx == idx)
                            break label;
                    } else self.next(.label).label_index;
                    try self.saveScratch(arity);
                    self.gotoLabel(label);
                    self.pushScratch(.value);
                },

                .@"return" => {
                    const arity = self.next(.arity);

                    try self.saveScratch(arity);
                    while (self.activation_link < self.stack.items.len)
                        self.gotoLabel(0);
                    self.completeFrame();
                    if (self.stack.items.len == 0)
                        break;
                    self.pushScratch(.value);
                },

                inline .call, .call_indirect => |tag| {
                    // Get the function definition and type.
                    const calling: *const FunctionInstance = switch (tag) {
                        .call => self.instance.functions[self.next(.function).func_index],
                        .call_indirect => blk: {
                            const typeidx = self.next(.type);
                            const tableidx = self.next(.table);
                            // (Steps 2-5) Asserts the existence of the table and get the table.
                            // Contrary to the specification, no assertion is made for the address
                            // itself existing because pointers are used rather than indices into
                            // the store.
                            const table = self.instance.tables[tableidx.table_index];
                            // (Steps 6-7) Asserts the existence of the type and get the expected
                            // type.
                            const expected_type = self.instance.types[typeidx.type_index];
                            // (Steps 8-9) Asserts the top value on the stack is an i32 and pop it.
                            const elemidx = self.pop(.value).i32;
                            // (Step 10) Trap if the element index is out of bounds.
                            if (elemidx >= table.entries.len)
                                return error.TrapOutOfBounds;
                            // (Steps 11-16) Extract the function reference, asserting the element
                            // is a function reference. Similar to steps 2-5, this is contrary to the
                            // specification in that no assertion is made for the address itself
                            // existing because pointers are used rather than indices into the
                            // store. Finally, trap if it is null.
                            const calling = table.entries[elemidx].funcref orelse
                                return error.TrapNullDereference;
                            // (Steps 17-18) Trap if the function signature doesn't match the
                            // expected type.
                            if (!expected_type.matches(calling.type.*))
                                return error.TrapTypeMismatch;
                            break :blk calling;
                        },
                        else => unreachable,
                    };

                    switch (calling.impl) {
                        .wasm => |impl| {
                            // TODO: Audit this for compliance to the specification's 4.4.10
                            // section.
                            const calling_params = calling.type.parameters;
                            // Buffer all the parameters into the scratch buffer.
                            // TODO: Remove use of the scratch buffer.
                            try self.scratch.ensureUnusedCapacity(self.alloc, calling_params.len);
                            var param_iter = std.mem.reverseIterator(calling_params);
                            while (param_iter.next()) |parameter_type| {
                                const parameter = self.pop(.value);
                                parameter_type.assert(parameter);
                                self.scratch.appendAssumeCapacity(parameter);
                            }

                            // Allocate space for the function's stack.
                            try self.allocFrame(impl.code);

                            // Push locals for each of the parameters and the declared locals in
                            // reverse order.
                            const locals = impl.code.locals[calling_params.len..];
                            var local_iter = std.mem.reverseIterator(locals);
                            while (local_iter.next()) |local|
                                self.push(.local, local.default());
                            // Reverse the locals order so they can be indexed by subtracting from
                            // the activation link.
                            for (self.scratch.items) |value|
                                self.push(.local, value);
                            self.scratch.items.len = 0;

                            // Push the frame.
                            self.save(.activation_link);
                            self.save(.program_counter);
                            self.save(.instance);
                            self.save(.activation);
                            self.activation_link = self.stack.items.len;
                            self.activation = impl.code.body.ptr;
                            self.instance = impl.module;
                            self.program_counter = 1;
                            self.save(.label_link);
                            self.push(.label_continuation, impl.code.body.len - 1);
                            self.label_link = self.stack.items.len;
                        },
                        .host => |impl| {
                            // This should compile out in release builds.
                            // TODO: Confirm this compiles out in release builds.
                            self.assertStack(calling.type.parameters);
                            // Call the host function with a restricted view of `self`. The host
                            // function should only be able to pop parameter values and push result
                            // values.
                            try impl.func(impl.context, @ptrCast(self));
                            // This should compile out in release builds.
                            // TODO: Confirm this compiles out in release builds.
                            self.assertStack(calling.type.results);
                        },
                    }
                },

                .drop => _ = self.pop(.value),
                .select => {
                    const condition = self.pop(.value).i32;
                    const right = self.pop(.value);
                    const left = self.pop(.value);

                    if (condition == 0) {
                        self.push(.value, right);
                    } else {
                        self.push(.value, left);
                    }
                },

                .@"local.get" => {
                    const localidx = self.next(.local);

                    const local = self.getLocal(localidx);
                    self.push(.value, local.*);
                },
                .@"local.set" => {
                    const localidx = self.next(.local);

                    const local = self.getLocal(localidx);
                    local.* = self.pop(.value);
                },
                .@"local.tee" => {
                    const localidx = self.next(.local);

                    const local = self.getLocal(localidx);
                    const value = self.pop(.value);
                    local.* = value;
                    self.push(.value, value);
                },
                .@"global.get" => {
                    const global = self.next(.global).global_index;
                    self.push(.value, self.instance.globals[global].value);
                },
                .@"global.set" => {
                    const global = self.next(.global).global_index;
                    const globalinst = self.instance.globals[global];
                    globalinst.value = self.pop(.value);
                    globalinst.type.type.assert(globalinst.value);
                },
                .@"table.get" => {
                    const tableidx = self.next(.table).table_index;
                    const tableinst = self.instance.tables[tableidx];

                    const elemidx = self.pop(.value).i32;
                    if (elemidx >= tableinst.entries.len)
                        return error.TrapOutOfBounds;
                    const elem = tableinst.entries[elemidx];
                    tableinst.type.assertType(elem);
                    self.push(.value, elem);
                },
                .@"table.set" => {
                    const tableidx = self.next(.table).table_index;
                    const tableinst = self.instance.tables[tableidx];

                    const elem = self.pop(.value);
                    tableinst.type.assertType(elem);
                    const elemidx = self.pop(.value).i32;
                    if (elemidx >= tableinst.entries.len)
                        return error.TrapOutOfBounds;
                    tableinst.entries[elemidx] = elem;
                },
                inline .@"i32.load",
                .@"i64.load",
                .@"f32.load",
                .@"f64.load",
                .@"i32.load8_s",
                .@"i32.load8_u",
                .@"i32.load16_s",
                .@"i32.load16_u",
                .@"i64.load8_s",
                .@"i64.load8_u",
                .@"i64.load16_s",
                .@"i64.load16_u",
                .@"i64.load32_s",
                .@"i64.load32_u",
                => |tag| {
                    const Unsigned, const T, const Stored, const value_tag = comptime switch (tag) {
                        .@"i32.load" => .{ u32, u32, u32, "i32" },
                        .@"i64.load" => .{ u64, u64, u64, "i64" },
                        .@"f32.load" => .{ f32, f32, f32, "f32" },
                        .@"f64.load" => .{ f64, f64, f64, "f64" },
                        .@"i32.load8_s" => .{ u32, i32, i8, "i32" },
                        .@"i32.load8_u" => .{ u32, u32, u8, "i32" },
                        .@"i32.load16_s" => .{ u32, i32, i16, "i32" },
                        .@"i32.load16_u" => .{ u32, u32, u16, "i32" },
                        .@"i64.load8_s" => .{ u64, i64, i8, "i64" },
                        .@"i64.load8_u" => .{ u64, u64, u8, "i64" },
                        .@"i64.load16_s" => .{ u64, i64, i16, "i64" },
                        .@"i64.load16_u" => .{ u64, u64, u16, "i64" },
                        .@"i64.load32_s" => .{ u64, i64, i32, "i64" },
                        .@"i64.load32_u" => .{ u64, u64, u32, "i64" },
                        else => unreachable,
                    };
                    const memory = self.instance.memories[0];
                    const static_offset: usize = self.next(.value).i32;
                    const dynamic_offset: usize = self.pop(.value).i32;
                    const effective_address = static_offset + dynamic_offset;
                    if (effective_address + @sizeOf(Stored) > memory.data.len)
                        return error.TrapOutOfBounds;
                    const bytes = memory.data[effective_address..][0..@sizeOf(Stored)];
                    trace.emit(.{ .exec = .{ .load = .{
                        .offset = effective_address,
                        .data = bytes,
                    } } });
                    const stored = std.mem.bytesToValue(Stored, bytes);
                    const native = std.mem.littleToNative(Stored, stored);
                    const expanded: T = native;
                    const value: Unsigned = @bitCast(expanded);
                    self.push(.value, @unionInit(Value, value_tag, value));
                },
                inline .@"i32.store",
                .@"i64.store",
                .@"f32.store",
                .@"f64.store",
                .@"i32.store8",
                .@"i32.store16",
                .@"i64.store8",
                .@"i64.store16",
                .@"i64.store32",
                => |tag| {
                    const T, const value_tag, const Truncated: ?type = comptime switch (tag) {
                        .@"i32.store" => .{ u32, "i32", null },
                        .@"i64.store" => .{ u64, "i64", null },
                        .@"f32.store" => .{ f32, "f32", null },
                        .@"f64.store" => .{ f64, "f64", null },
                        .@"i32.store8" => .{ u32, "i32", u8 },
                        .@"i32.store16" => .{ u32, "i32", u16 },
                        .@"i64.store8" => .{ u64, "i64", u8 },
                        .@"i64.store16" => .{ u64, "i64", u16 },
                        .@"i64.store32" => .{ u64, "i64", u32 },
                        else => unreachable,
                    };
                    const memory = self.instance.memories[0];
                    const static_offset: usize = self.next(.value).i32;
                    const value = @field(self.pop(.value), value_tag);
                    const dynamic_offset: usize = self.pop(.value).i32;
                    const effective_address = static_offset + dynamic_offset;
                    const bytes_len = @sizeOf(Truncated orelse T);
                    const end_address = effective_address + bytes_len;
                    if (end_address > memory.data.len)
                        return error.TrapOutOfBounds;
                    const bytes = if (comptime Truncated) |U| blk: {
                        const truncated: U = @truncate(value);
                        const little = std.mem.nativeToLittle(U, truncated);
                        break :blk std.mem.asBytes(&little);
                    } else std.mem.asBytes(&std.mem.nativeToLittle(T, value));
                    trace.emit(.{ .exec = .{ .store = .{
                        .offset = effective_address,
                        .data = bytes,
                    } } });
                    @memcpy(memory.data[effective_address..end_address], bytes);
                },

                .@"memory.size" => {
                    const memory = self.instance.memories[0];
                    self.push(.value, .{ .i32 = @intCast(@divExact(memory.data.len, page_size)) });
                },
                .@"memory.grow" => {
                    const memory = self.instance.memories[0];
                    std.debug.assert(memory.data.len % page_size == 0);
                    const pages: usize = @divExact(memory.data.len, page_size);
                    const new_pages: usize = pages + self.pop(.value).i32;
                    const result: Value = blk: {
                        if (memory.limits.max) |max| {
                            if (new_pages > max) {
                                break :blk GROW_ERROR_VALUE;
                            }
                        }
                        if (new_pages > std.math.maxInt(u16))
                            break :blk GROW_ERROR_VALUE;
                        if (store.alloc.realloc(memory.data, new_pages * page_size)) |new| {
                            memory.data = new;
                            @memset(new[pages * page_size ..], 0);
                            memory.limits.min = @intCast(new_pages);
                            break :blk .{ .i32 = @intCast(pages) };
                        } else |_| {
                            break :blk GROW_ERROR_VALUE;
                        }
                    };
                    self.push(.value, result);
                },

                .@"i32.const" => self.push(.value, .{ .i32 = self.next(.value).i32 }),
                .@"i64.const" => self.push(.value, .{ .i64 = self.next(.value).i64 }),
                .@"f32.const" => self.push(.value, .{ .f32 = self.next(.value).f32 }),
                .@"f64.const" => self.push(.value, .{ .f64 = self.next(.value).f64 }),
                .@"i32.eqz" => {
                    const operand = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = @intFromBool(operand == 0) });
                },
                .@"i32.eq" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = @intFromBool(left == right) });
                },
                .@"i32.ne" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = @intFromBool(left != right) });
                },
                .@"i32.lt_s" => {
                    const right: i32 = @bitCast(self.pop(.value).i32);
                    const left: i32 = @bitCast(self.pop(.value).i32);
                    self.push(.value, .{ .i32 = @intFromBool(left < right) });
                },
                .@"i32.lt_u" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = @intFromBool(left < right) });
                },
                .@"i32.gt_s" => {
                    const right: i32 = @bitCast(self.pop(.value).i32);
                    const left: i32 = @bitCast(self.pop(.value).i32);
                    self.push(.value, .{ .i32 = @intFromBool(left > right) });
                },
                .@"i32.gt_u" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = @intFromBool(left > right) });
                },
                .@"i32.le_s" => {
                    const right: i32 = @bitCast(self.pop(.value).i32);
                    const left: i32 = @bitCast(self.pop(.value).i32);
                    self.push(.value, .{ .i32 = @intFromBool(left <= right) });
                },
                .@"i32.le_u" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = @intFromBool(left <= right) });
                },
                .@"i32.ge_s" => {
                    const right: i32 = @bitCast(self.pop(.value).i32);
                    const left: i32 = @bitCast(self.pop(.value).i32);
                    self.push(.value, .{ .i32 = @intFromBool(left >= right) });
                },
                .@"i32.ge_u" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = @intFromBool(left >= right) });
                },
                .@"i64.eqz" => {
                    const operand = self.pop(.value).i64;
                    self.push(.value, .{ .i32 = @intFromBool(operand == 0) });
                },
                .@"i64.eq" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    self.push(.value, .{ .i32 = @intFromBool(left == right) });
                },
                .@"i64.ne" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    self.push(.value, .{ .i32 = @intFromBool(left != right) });
                },
                .@"i64.lt_s" => {
                    const right: i64 = @bitCast(self.pop(.value).i64);
                    const left: i64 = @bitCast(self.pop(.value).i64);
                    self.push(.value, .{ .i32 = @intFromBool(left < right) });
                },
                .@"i64.lt_u" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    self.push(.value, .{ .i32 = @intFromBool(left < right) });
                },
                .@"i64.gt_s" => {
                    const right: i64 = @bitCast(self.pop(.value).i64);
                    const left: i64 = @bitCast(self.pop(.value).i64);
                    self.push(.value, .{ .i32 = @intFromBool(left > right) });
                },
                .@"i64.gt_u" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    self.push(.value, .{ .i32 = @intFromBool(left > right) });
                },
                .@"i64.le_s" => {
                    const right: i64 = @bitCast(self.pop(.value).i64);
                    const left: i64 = @bitCast(self.pop(.value).i64);
                    self.push(.value, .{ .i32 = @intFromBool(left <= right) });
                },
                .@"i64.le_u" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    self.push(.value, .{ .i32 = @intFromBool(left <= right) });
                },
                .@"i64.ge_s" => {
                    const right: i64 = @bitCast(self.pop(.value).i64);
                    const left: i64 = @bitCast(self.pop(.value).i64);
                    self.push(.value, .{ .i32 = @intFromBool(left >= right) });
                },
                .@"i64.ge_u" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    self.push(.value, .{ .i32 = @intFromBool(left >= right) });
                },
                .@"f32.eq" => {
                    const right = self.pop(.value).f32;
                    const left = self.pop(.value).f32;
                    self.push(.value, .{ .i32 = @intFromBool(left == right) });
                },
                .@"f32.ne" => {
                    const right = self.pop(.value).f32;
                    const left = self.pop(.value).f32;
                    self.push(.value, .{ .i32 = @intFromBool(left != right) });
                },
                .@"f32.lt" => {
                    const right = self.pop(.value).f32;
                    const left = self.pop(.value).f32;
                    self.push(.value, .{ .i32 = @intFromBool(left < right) });
                },
                .@"f32.gt" => {
                    const right = self.pop(.value).f32;
                    const left = self.pop(.value).f32;
                    self.push(.value, .{ .i32 = @intFromBool(left > right) });
                },
                .@"f32.le" => {
                    const right = self.pop(.value).f32;
                    const left = self.pop(.value).f32;
                    self.push(.value, .{ .i32 = @intFromBool(left <= right) });
                },
                .@"f32.ge" => {
                    const right = self.pop(.value).f32;
                    const left = self.pop(.value).f32;
                    self.push(.value, .{ .i32 = @intFromBool(left >= right) });
                },
                .@"f64.eq" => {
                    const right = self.pop(.value).f64;
                    const left = self.pop(.value).f64;
                    self.push(.value, .{ .i32 = @intFromBool(left == right) });
                },
                .@"f64.ne" => {
                    const right = self.pop(.value).f64;
                    const left = self.pop(.value).f64;
                    self.push(.value, .{ .i32 = @intFromBool(left != right) });
                },
                .@"f64.lt" => {
                    const right = self.pop(.value).f64;
                    const left = self.pop(.value).f64;
                    self.push(.value, .{ .i32 = @intFromBool(left < right) });
                },
                .@"f64.gt" => {
                    const right = self.pop(.value).f64;
                    const left = self.pop(.value).f64;
                    self.push(.value, .{ .i32 = @intFromBool(left > right) });
                },
                .@"f64.le" => {
                    const right = self.pop(.value).f64;
                    const left = self.pop(.value).f64;
                    self.push(.value, .{ .i32 = @intFromBool(left <= right) });
                },
                .@"f64.ge" => {
                    const right = self.pop(.value).f64;
                    const left = self.pop(.value).f64;
                    self.push(.value, .{ .i32 = @intFromBool(left >= right) });
                },
                .@"i32.clz" => {
                    const operand = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = @clz(operand) });
                },
                .@"i32.ctz" => {
                    const operand = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = @ctz(operand) });
                },
                .@"i32.popcnt" => {
                    const operand = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = @popCount(operand) });
                },
                .@"i32.add" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = left +% right });
                },
                .@"i32.sub" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = left -% right });
                },
                .@"i32.mul" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = left *% right });
                },
                .@"i32.div_s" => {
                    const denominator: i32 = @bitCast(self.pop(.value).i32);
                    const numerator: i32 = @bitCast(self.pop(.value).i32);
                    if (denominator == 0)
                        return error.TrapDivisionByZero;
                    if (numerator == std.math.minInt(i32) and denominator == -1)
                        return error.TrapOverflow;
                    const result = @divTrunc(numerator, denominator);
                    self.push(.value, .{ .i32 = @bitCast(result) });
                },
                .@"i32.div_u" => {
                    const denominator = self.pop(.value).i32;
                    const numerator = self.pop(.value).i32;
                    if (denominator == 0)
                        return error.TrapDivisionByZero;
                    self.push(.value, .{ .i32 = @divTrunc(numerator, denominator) });
                },
                .@"i32.rem_s" => {
                    const denominator: i32 = @bitCast(self.pop(.value).i32);
                    const numerator: i32 = @bitCast(self.pop(.value).i32);
                    if (denominator == 0)
                        return error.TrapDivisionByZero;
                    // TODO: This seems like an ugly hack. Is there a better way of implementing C-like
                    // remainder operator better while still respecting the edge cases such as minimum
                    // divided by -1?
                    const result = if (numerator == std.math.minInt(i32) and denominator == -1)
                        0
                    else
                        std.zig.c_translation.signedRemainder(numerator, denominator);
                    self.push(.value, .{ .i32 = @bitCast(result) });
                },
                .@"i32.rem_u" => {
                    const denominator = self.pop(.value).i32;
                    const numerator = self.pop(.value).i32;
                    if (denominator == 0)
                        return error.TrapDivisionByZero;
                    self.push(.value, .{ .i32 = numerator % denominator });
                },
                .@"i32.and" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = left & right });
                },
                .@"i32.or" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = left | right });
                },
                .@"i32.xor" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = left ^ right });
                },
                .@"i32.shl" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = left << @intCast(right % 32) });
                },
                .@"i32.shr_s" => {
                    const right = self.pop(.value).i32;
                    const left: i32 = @bitCast(self.pop(.value).i32);
                    const result = left >> @intCast(right % 32);
                    self.push(.value, .{ .i32 = @bitCast(result) });
                },
                .@"i32.shr_u" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    const result = left >> @intCast(right % 32);
                    self.push(.value, .{ .i32 = @bitCast(result) });
                },
                .@"i32.rotl" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = std.math.rotl(u32, left, right % 32) });
                },
                .@"i32.rotr" => {
                    const right = self.pop(.value).i32;
                    const left = self.pop(.value).i32;
                    self.push(.value, .{ .i32 = std.math.rotr(u32, left, right % 32) });
                },
                .@"i64.clz" => {
                    const operand = self.pop(.value).i64;
                    self.push(.value, .{ .i64 = @clz(operand) });
                },
                .@"i64.ctz" => {
                    const operand = self.pop(.value).i64;
                    self.push(.value, .{ .i64 = @ctz(operand) });
                },
                .@"i64.popcnt" => {
                    const operand = self.pop(.value).i64;
                    self.push(.value, .{ .i64 = @popCount(operand) });
                },
                .@"i64.add" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    self.push(.value, .{ .i64 = left +% right });
                },
                .@"i64.sub" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    self.push(.value, .{ .i64 = left -% right });
                },
                .@"i64.mul" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    self.push(.value, .{ .i64 = left *% right });
                },
                .@"i64.div_s" => {
                    const denominator: i64 = @bitCast(self.pop(.value).i64);
                    const numerator: i64 = @bitCast(self.pop(.value).i64);
                    if (denominator == 0)
                        return error.TrapDivisionByZero;
                    if (numerator == std.math.minInt(i64) and denominator == -1)
                        return error.TrapOverflow;
                    const result = @divTrunc(numerator, denominator);
                    self.push(.value, .{ .i64 = @bitCast(result) });
                },
                .@"i64.div_u" => {
                    const denominator = self.pop(.value).i64;
                    const numerator = self.pop(.value).i64;
                    if (denominator == 0)
                        return error.TrapDivisionByZero;
                    self.push(.value, .{ .i64 = @divTrunc(numerator, denominator) });
                },
                .@"i64.rem_s" => {
                    const denominator: i64 = @bitCast(self.pop(.value).i64);
                    const numerator: i64 = @bitCast(self.pop(.value).i64);
                    if (denominator == 0)
                        return error.TrapDivisionByZero;
                    // TODO: This seems like an ugly hack. Is there a better way of implementing C-like
                    // remainder operator better while still respecting the edge cases such as minimum
                    // divided by -1?
                    const result = if (numerator == std.math.minInt(i64) and denominator == -1)
                        0
                    else
                        std.zig.c_translation.signedRemainder(numerator, denominator);
                    self.push(.value, .{ .i64 = @bitCast(result) });
                },
                .@"i64.rem_u" => {
                    const denominator = self.pop(.value).i64;
                    const numerator = self.pop(.value).i64;
                    if (denominator == 0)
                        return error.TrapDivisionByZero;
                    self.push(.value, .{ .i64 = numerator % denominator });
                },
                .@"i64.and" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    self.push(.value, .{ .i64 = left & right });
                },
                .@"i64.or" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    self.push(.value, .{ .i64 = left | right });
                },
                .@"i64.xor" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    self.push(.value, .{ .i64 = left ^ right });
                },
                .@"i64.shl" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    self.push(.value, .{ .i64 = left << @intCast(right % 64) });
                },
                .@"i64.shr_s" => {
                    const right = self.pop(.value).i64;
                    const left: i64 = @bitCast(self.pop(.value).i64);
                    const result = left >> @intCast(right % 64);
                    self.push(.value, .{ .i64 = @bitCast(result) });
                },
                .@"i64.shr_u" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    const result = left >> @intCast(right % 64);
                    self.push(.value, .{ .i64 = @bitCast(result) });
                },
                .@"i64.rotl" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    self.push(.value, .{ .i64 = std.math.rotl(u64, left, right % 64) });
                },
                .@"i64.rotr" => {
                    const right = self.pop(.value).i64;
                    const left = self.pop(.value).i64;
                    self.push(.value, .{ .i64 = std.math.rotr(u64, left, right % 64) });
                },
                .@"f32.abs" => {
                    const operand = self.pop(.value).f32;
                    self.push(.value, .{ .f32 = @abs(operand) });
                },
                .@"f32.neg" => {
                    const operand = self.pop(.value).f32;
                    self.push(.value, .{ .f32 = -operand });
                },
                .@"f32.ceil" => {
                    const operand = self.pop(.value).f32;
                    self.push(.value, .{ .f32 = @ceil(operand) });
                },
                .@"f32.floor" => {
                    const operand = self.pop(.value).f32;
                    self.push(.value, .{ .f32 = @floor(operand) });
                },
                .@"f32.trunc" => {
                    const operand = self.pop(.value).f32;
                    self.push(.value, .{ .f32 = @trunc(operand) });
                },
                .@"f32.nearest" => {
                    const operand = self.pop(.value).f32;
                    const split = std.math.modf(operand);
                    const result = split.ipart + if (@abs(split.fpart) == 0.5)
                        @rem(split.ipart, 2.0)
                    else
                        0.0;
                    self.push(.value, .{ .f32 = result });
                },
                .@"f32.sqrt" => {
                    const operand = self.pop(.value).f32;
                    self.push(.value, .{ .f32 = @sqrt(operand) });
                },
                .@"f32.add" => {
                    const right = self.pop(.value).f32;
                    const left = self.pop(.value).f32;
                    self.push(.value, .{ .f32 = left + right });
                },
                .@"f32.sub" => {
                    const right = self.pop(.value).f32;
                    const left = self.pop(.value).f32;
                    self.push(.value, .{ .f32 = left - right });
                },
                .@"f32.mul" => {
                    const right = self.pop(.value).f32;
                    const left = self.pop(.value).f32;
                    self.push(.value, .{ .f32 = left * right });
                },
                .@"f32.div" => {
                    const right = self.pop(.value).f32;
                    const left = self.pop(.value).f32;
                    self.push(.value, .{ .f32 = left / right });
                },
                .@"f32.min" => {
                    const right = self.pop(.value).f32;
                    const left = self.pop(.value).f32;
                    const result = if (std.math.isNan(left))
                        left
                    else if (std.math.isNan(right))
                        right
                    else
                        @min(left, right);
                    self.push(.value, .{ .f32 = result });
                },
                .@"f32.max" => {
                    const right = self.pop(.value).f32;
                    const left = self.pop(.value).f32;
                    const result = if (std.math.isNan(left))
                        left
                    else if (std.math.isNan(right))
                        right
                    else
                        @max(left, right);
                    self.push(.value, .{ .f32 = result });
                },
                .@"f32.copysign" => {
                    const right = self.pop(.value).f32;
                    const left = self.pop(.value).f32;
                    self.push(.value, .{ .f32 = std.math.copysign(left, right) });
                },
                .@"f64.abs" => {
                    const operand = self.pop(.value).f64;
                    self.push(.value, .{ .f64 = @abs(operand) });
                },
                .@"f64.neg" => {
                    const operand = self.pop(.value).f64;
                    self.push(.value, .{ .f64 = -operand });
                },
                .@"f64.ceil" => {
                    const operand = self.pop(.value).f64;
                    self.push(.value, .{ .f64 = @ceil(operand) });
                },
                .@"f64.floor" => {
                    const operand = self.pop(.value).f64;
                    self.push(.value, .{ .f64 = @floor(operand) });
                },
                .@"f64.trunc" => {
                    const operand = self.pop(.value).f64;
                    self.push(.value, .{ .f64 = @trunc(operand) });
                },
                .@"f64.nearest" => {
                    const operand = self.pop(.value).f64;
                    const split = std.math.modf(operand);
                    const result = split.ipart + if (@abs(split.fpart) == 0.5)
                        @rem(split.ipart, 2.0)
                    else
                        0.0;
                    self.push(.value, .{ .f64 = result });
                },
                .@"f64.sqrt" => {
                    const operand = self.pop(.value).f64;
                    self.push(.value, .{ .f64 = @sqrt(operand) });
                },
                .@"f64.add" => {
                    const right = self.pop(.value).f64;
                    const left = self.pop(.value).f64;
                    self.push(.value, .{ .f64 = left + right });
                },
                .@"f64.sub" => {
                    const right = self.pop(.value).f64;
                    const left = self.pop(.value).f64;
                    self.push(.value, .{ .f64 = left - right });
                },
                .@"f64.mul" => {
                    const right = self.pop(.value).f64;
                    const left = self.pop(.value).f64;
                    self.push(.value, .{ .f64 = left * right });
                },
                .@"f64.div" => {
                    const right = self.pop(.value).f64;
                    const left = self.pop(.value).f64;
                    self.push(.value, .{ .f64 = left / right });
                },
                .@"f64.min" => {
                    const right = self.pop(.value).f64;
                    const left = self.pop(.value).f64;
                    const result = if (std.math.isNan(left))
                        left
                    else if (std.math.isNan(right))
                        right
                    else
                        @min(left, right);
                    self.push(.value, .{ .f64 = result });
                },
                .@"f64.max" => {
                    const right = self.pop(.value).f64;
                    const left = self.pop(.value).f64;
                    const result = if (std.math.isNan(left))
                        left
                    else if (std.math.isNan(right))
                        right
                    else
                        @max(left, right);
                    self.push(.value, .{ .f64 = result });
                },
                .@"f64.copysign" => {
                    const right = self.pop(.value).f64;
                    const left = self.pop(.value).f64;
                    self.push(.value, .{ .f64 = std.math.copysign(left, right) });
                },
                .@"i32.wrap_i64" => {
                    const operand = self.pop(.value).i64;
                    self.push(.value, .{ .i32 = @truncate(operand) });
                },
                .@"i32.trunc_f32_s" => {
                    const operand = self.pop(.value).f32;
                    if (std.math.isNan(operand) or std.math.isInf(operand))
                        return error.TrapOverflow;
                    const truncated = @trunc(operand);
                    // TODO: Derive these magic numbers from first principles. These magic numbers were
                    // simply copied from the "wasm3" project.
                    if (truncated <= -2147483904.0 or 2147483648.0 <= truncated)
                        return error.TrapOverflow;
                    const result: i32 = @intFromFloat(truncated);
                    self.push(.value, .{ .i32 = @bitCast(result) });
                },
                .@"i32.trunc_f32_u" => {
                    const operand = self.pop(.value).f32;
                    if (std.math.isNan(operand) or std.math.isInf(operand))
                        return error.TrapOverflow;
                    const truncated = @trunc(operand);
                    // TODO: Derive these magic numbers from first principles. These magic numbers were
                    // simply copied from the "wasm3" project.
                    if (truncated <= -1.0 or 4294967296.0 <= truncated)
                        return error.TrapOverflow;
                    const result: u32 = @intFromFloat(truncated);
                    self.push(.value, .{ .i32 = result });
                },
                .@"i32.trunc_f64_s" => {
                    const operand = self.pop(.value).f64;
                    if (std.math.isNan(operand) or std.math.isInf(operand))
                        return error.TrapOverflow;
                    const truncated = @trunc(operand);
                    // TODO: Derive these magic numbers from first principles. These magic numbers were
                    // simply copied from the "wasm3" project.
                    if (truncated <= -2147483649.0 or 2147483648.0 <= truncated)
                        return error.TrapOverflow;
                    const result: i32 = @intFromFloat(truncated);
                    self.push(.value, .{ .i32 = @bitCast(result) });
                },
                .@"i32.trunc_f64_u" => {
                    const operand = self.pop(.value).f64;
                    if (std.math.isNan(operand) or std.math.isInf(operand))
                        return error.TrapOverflow;
                    const truncated = @trunc(operand);
                    // TODO: Derive these magic numbers from first principles. These magic numbers were
                    // simply copied from the "wasm3" project.
                    if (truncated <= -1.0 or 4294967296.0 <= truncated)
                        return error.TrapOverflow;
                    const result: u32 = @intFromFloat(truncated);
                    self.push(.value, .{ .i32 = result });
                },
                .@"i64.extend_i32_s" => {
                    const operand: i32 = @bitCast(self.pop(.value).i32);
                    const result: i64 = @intCast(operand);
                    self.push(.value, .{ .i64 = @bitCast(result) });
                },
                .@"i64.extend_i32_u" => {
                    const operand = self.pop(.value).i32;
                    const result: u64 = @intCast(operand);
                    self.push(.value, .{ .i64 = result });
                },
                .@"i64.trunc_f32_s" => {
                    const operand = self.pop(.value).f32;
                    if (std.math.isNan(operand) or std.math.isInf(operand))
                        return error.TrapOverflow;
                    const truncated = @trunc(operand);
                    // TODO: Derive these magic numbers from first principles. These magic numbers were
                    // simply copied from the "wasm3" project.
                    if (truncated <= -9223373136366403584.0 or 9223372036854775808.0 <= truncated)
                        return error.TrapOverflow;
                    const result: i64 = @intFromFloat(truncated);
                    self.push(.value, .{ .i64 = @bitCast(result) });
                },
                .@"i64.trunc_f32_u" => {
                    const operand = self.pop(.value).f32;
                    if (std.math.isNan(operand) or std.math.isInf(operand))
                        return error.TrapOverflow;
                    const truncated = @trunc(operand);
                    // TODO: Derive these magic numbers from first principles. These magic numbers were
                    // simply copied from the "wasm3" project.
                    if (truncated <= -1.0 or 18446744073709551616.0 <= truncated)
                        return error.TrapOverflow;
                    const result: u64 = @intFromFloat(truncated);
                    self.push(.value, .{ .i64 = result });
                },
                .@"i64.trunc_f64_s" => {
                    const operand = self.pop(.value).f64;
                    if (std.math.isNan(operand) or std.math.isInf(operand))
                        return error.TrapOverflow;
                    const truncated = @trunc(operand);
                    // TODO: Derive these magic numbers from first principles. These magic numbers were
                    // simply copied from the "wasm3" project.
                    if (truncated <= -9223372036854777856.0 or 9223372036854775808.0 <= truncated)
                        return error.TrapOverflow;
                    const result: i64 = @intFromFloat(truncated);
                    self.push(.value, .{ .i64 = @bitCast(result) });
                },
                .@"i64.trunc_f64_u" => {
                    const operand = self.pop(.value).f64;
                    if (std.math.isNan(operand) or std.math.isInf(operand))
                        return error.TrapOverflow;
                    const truncated = @trunc(operand);
                    // TODO: Derive these magic numbers from first principles. These magic numbers were
                    // simply copied from the "wasm3" project.
                    if (truncated <= -1.0 or 18446744073709551616.0 <= truncated)
                        return error.TrapOverflow;
                    const result: u64 = @intFromFloat(truncated);
                    self.push(.value, .{ .i64 = result });
                },
                .@"f32.convert_i32_s" => {
                    const operand: i32 = @bitCast(self.pop(.value).i32);
                    self.push(.value, .{ .f32 = @floatFromInt(operand) });
                },
                .@"f32.convert_i32_u" => {
                    const operand = self.pop(.value).i32;
                    self.push(.value, .{ .f32 = @floatFromInt(operand) });
                },
                .@"f32.convert_i64_s" => {
                    const operand: i64 = @bitCast(self.pop(.value).i64);
                    self.push(.value, .{ .f32 = @floatFromInt(operand) });
                },
                .@"f32.convert_i64_u" => {
                    const operand = self.pop(.value).i64;
                    self.push(.value, .{ .f32 = @floatFromInt(operand) });
                },
                .@"f32.demote_f64" => {
                    const operand = self.pop(.value).f64;
                    self.push(.value, .{ .f32 = @floatCast(operand) });
                },
                .@"f64.convert_i32_s" => {
                    const operand: i32 = @bitCast(self.pop(.value).i32);
                    self.push(.value, .{ .f64 = @floatFromInt(operand) });
                },
                .@"f64.convert_i32_u" => {
                    const operand = self.pop(.value).i32;
                    self.push(.value, .{ .f64 = @floatFromInt(operand) });
                },
                .@"f64.convert_i64_s" => {
                    const operand: i64 = @bitCast(self.pop(.value).i64);
                    self.push(.value, .{ .f64 = @floatFromInt(operand) });
                },
                .@"f64.convert_i64_u" => {
                    const operand = self.pop(.value).i64;
                    self.push(.value, .{ .f64 = @floatFromInt(operand) });
                },
                .@"f64.promote_f32" => {
                    const operand = self.pop(.value).f32;
                    self.push(.value, .{ .f64 = @floatCast(operand) });
                },
                .@"i32.reinterpret_f32" => {
                    const operand = self.pop(.value).f32;
                    self.push(.value, .{ .i32 = @bitCast(operand) });
                },
                .@"i64.reinterpret_f64" => {
                    const operand = self.pop(.value).f64;
                    self.push(.value, .{ .i64 = @bitCast(operand) });
                },
                .@"f32.reinterpret_i32" => {
                    const operand = self.pop(.value).i32;
                    self.push(.value, .{ .f32 = @bitCast(operand) });
                },
                .@"f64.reinterpret_i64" => {
                    const operand = self.pop(.value).i64;
                    self.push(.value, .{ .f64 = @bitCast(operand) });
                },
                .@"i32.extend8_s" => {
                    const operand = self.pop(.value).i32;
                    const unsigned: u8 = @truncate(operand);
                    const signed: i8 = @bitCast(unsigned);
                    const result: i32 = @intCast(signed);
                    self.push(.value, .{ .i32 = @bitCast(result) });
                },
                .@"i32.extend16_s" => {
                    const operand = self.pop(.value).i32;
                    const unsigned: u16 = @truncate(operand);
                    const signed: i16 = @bitCast(unsigned);
                    const result: i32 = @intCast(signed);
                    self.push(.value, .{ .i32 = @bitCast(result) });
                },
                .@"i64.extend8_s" => {
                    const operand = self.pop(.value).i64;
                    const unsigned: u8 = @truncate(operand);
                    const signed: i8 = @bitCast(unsigned);
                    const result: i64 = @intCast(signed);
                    self.push(.value, .{ .i64 = @bitCast(result) });
                },
                .@"i64.extend16_s" => {
                    const operand = self.pop(.value).i64;
                    const unsigned: u16 = @truncate(operand);
                    const signed: i16 = @bitCast(unsigned);
                    const result: i64 = @intCast(signed);
                    self.push(.value, .{ .i64 = @bitCast(result) });
                },
                .@"i64.extend32_s" => {
                    const operand = self.pop(.value).i64;
                    const unsigned: u32 = @truncate(operand);
                    const signed: i32 = @bitCast(unsigned);
                    const result: i64 = @intCast(signed);
                    self.push(.value, .{ .i64 = @bitCast(result) });
                },

                .@"ref.null" => {
                    const value = self.next(.value);
                    self.push(.value, value);
                },
                .@"ref.is_null" => {
                    const reftype = self.next(.reftype);
                    const ref = self.pop(.value);
                    self.push(.value, .{ .i32 = @intFromBool(switch (reftype) {
                        .externref => ref.externref == null,
                        .funcref => ref.funcref == null,
                    }) });
                },
                .@"ref.func" => {
                    const funcidx = self.next(.function);
                    const funcinst = self.instance.functions[funcidx.func_index];
                    self.push(.value, .{ .funcref = funcinst });
                },

                .@"i32.trunc_sat_f32_s" => {
                    const operand = self.pop(.value).f32;
                    const result = std.math.lossyCast(i32, operand);
                    self.push(.value, .{ .i32 = @bitCast(result) });
                },
                .@"i32.trunc_sat_f32_u" => {
                    const operand = self.pop(.value).f32;
                    const result = std.math.lossyCast(u32, operand);
                    self.push(.value, .{ .i32 = result });
                },
                .@"i32.trunc_sat_f64_s" => {
                    const operand = self.pop(.value).f64;
                    const result = std.math.lossyCast(i32, operand);
                    self.push(.value, .{ .i32 = @bitCast(result) });
                },
                .@"i32.trunc_sat_f64_u" => {
                    const operand = self.pop(.value).f64;
                    const result = std.math.lossyCast(u32, operand);
                    self.push(.value, .{ .i32 = result });
                },
                .@"i64.trunc_sat_f32_s" => {
                    const operand = self.pop(.value).f32;
                    const result = std.math.lossyCast(i64, operand);
                    self.push(.value, .{ .i64 = @bitCast(result) });
                },
                .@"i64.trunc_sat_f32_u" => {
                    const operand = self.pop(.value).f32;
                    const result = std.math.lossyCast(u64, operand);
                    self.push(.value, .{ .i64 = result });
                },
                .@"i64.trunc_sat_f64_s" => {
                    const operand = self.pop(.value).f64;
                    const result = std.math.lossyCast(i64, operand);
                    self.push(.value, .{ .i64 = @bitCast(result) });
                },
                .@"i64.trunc_sat_f64_u" => {
                    const operand = self.pop(.value).f64;
                    const result = std.math.lossyCast(u64, operand);
                    self.push(.value, .{ .i64 = result });
                },
                .@"memory.init" => {
                    const memory = self.instance.memories[0];
                    const dataidx = self.next(.data);
                    const datainst = self.instance.data[dataidx.data_index];

                    const count = self.pop(.value).i32;
                    const data_start = self.pop(.value).i32;
                    const memory_start = self.pop(.value).i32;

                    const data_end, const data_ov = @addWithOverflow(data_start, count);
                    if (data_ov != 0 or data_end > datainst.data.len)
                        return error.TrapOutOfBounds;
                    const memory_end, const table_ov = @addWithOverflow(memory_start, count);
                    if (table_ov != 0 or memory_end > memory.data.len)
                        return error.TrapOutOfBounds;
                    const bytes = datainst.data[data_start..data_end];
                    trace.emit(.{ .exec = .{ .store = .{
                        .offset = memory_start,
                        .data = bytes,
                    } } });
                    if (count > 0) {
                        @memcpy(memory.data[memory_start..memory_end], bytes);
                    }
                },
                .@"data.drop" => {
                    const dataidx = self.next(.data);
                    const datainst = self.instance.data[dataidx.data_index];
                    // `Module` owns the data.
                    datainst.data = &.{};
                },
                .@"memory.copy" => {
                    const memory = self.instance.memories[0];

                    const count = self.pop(.value).i32;
                    const src_start = self.pop(.value).i32;
                    const dest_start = self.pop(.value).i32;

                    const src_end, const src_ov = @addWithOverflow(src_start, count);
                    if (src_ov != 0 or src_end > memory.data.len)
                        return error.TrapOutOfBounds;
                    const dest_end, const dest_ov = @addWithOverflow(dest_start, count);
                    if (dest_ov != 0 or dest_end > memory.data.len)
                        return error.TrapOutOfBounds;

                    const src = memory.data[src_start..src_end];
                    const dest = memory.data[dest_start..dest_end];
                    trace.emit(.{ .exec = .{ .copy = .{
                        .source = src_start,
                        .destination = dest_start,
                        .data = src,
                    } } });
                    if (@intFromPtr(dest.ptr) <= @intFromPtr(src.ptr)) {
                        std.mem.copyForwards(u8, dest, src);
                    } else {
                        std.mem.copyBackwards(u8, dest, src);
                    }
                },
                .@"memory.fill" => {
                    const memory = self.instance.memories[0];

                    const count = self.pop(.value).i32;
                    const fill: u8 = @truncate(self.pop(.value).i32);
                    const dest_start = self.pop(.value).i32;

                    const dest_end, const dest_ov = @addWithOverflow(dest_start, count);
                    if (dest_ov != 0 or dest_end > memory.data.len)
                        return error.TrapOutOfBounds;
                    const dest = memory.data[dest_start..dest_end];
                    trace.emit(.{ .exec = .{ .fill = .{
                        .offset = dest_start,
                        .length = count,
                        .byte = fill,
                    } } });
                    @memset(dest, fill);
                },

                .@"table.init" => {
                    // The specification is rather verbose for this one. While the specification's
                    // execution description 27 steps, it essentially just describes bounds checking
                    // the element and table slicing then recursively calling itself. Presumably,
                    // this is because it is simpler to formally define. Here, we just bounds check
                    // and use `@memcpy`.
                    const elemidx = self.next(.element);
                    const tableidx = self.next(.table);
                    const eleminst = self.instance.elements[elemidx.elem_index];
                    const tableinst = self.instance.tables[tableidx.table_index];
                    std.debug.assert(eleminst.type == tableinst.type.type);

                    const count = self.pop(.value).i32;
                    const elem_start = self.pop(.value).i32;
                    const table_start = self.pop(.value).i32;

                    const elem_end, const elem_ov = @addWithOverflow(elem_start, count);
                    if (elem_ov != 0 or elem_end > eleminst.entries.len)
                        return error.TrapOutOfBounds;
                    const table_end, const table_ov = @addWithOverflow(table_start, count);
                    if (table_ov != 0 or table_end > tableinst.entries.len)
                        return error.TrapOutOfBounds;
                    if (count > 0) {
                        @memcpy(
                            tableinst.entries[table_start..table_end],
                            eleminst.entries[elem_start..elem_end],
                        );
                    }
                },
                .@"elem.drop" => {
                    const elemidx = self.next(.element);
                    const eleminst = self.instance.elements[elemidx.elem_index];

                    store.drop(eleminst.entries);
                    eleminst.entries = &.{};
                },
                .@"table.copy" => {
                    const destidx = self.next(.table);
                    const srcidx = self.next(.table);
                    const destinst = self.instance.tables[destidx.table_index];
                    const srcinst = self.instance.tables[srcidx.table_index];

                    const count = self.pop(.value).i32;
                    const src_start = self.pop(.value).i32;
                    const dest_start = self.pop(.value).i32;
                    const src_end = std.math.add(u32, src_start, count) catch
                        return error.TrapOutOfBounds;
                    const dest_end = std.math.add(u32, dest_start, count) catch
                        return error.TrapOutOfBounds;

                    if (src_end > srcinst.entries.len or dest_end > destinst.entries.len)
                        return error.TrapOutOfBounds;
                    if (count > 0) {
                        const dest = destinst.entries[dest_start..dest_end];
                        const src = srcinst.entries[src_start..src_end];
                        if (destinst == srcinst) {
                            if (@intFromPtr(dest.ptr) <= @intFromPtr(src.ptr)) {
                                std.mem.copyForwards(Value, dest, src);
                            } else {
                                std.mem.copyBackwards(Value, dest, src);
                            }
                        } else {
                            @memcpy(dest, src);
                        }
                    }
                },
                .@"table.grow" => {
                    const tableidx = self.next(.table);
                    const tableinst = self.instance.tables[tableidx.table_index];

                    const count = self.pop(.value).i32;
                    const ref = self.pop(.value);
                    tableinst.type.assertType(ref);

                    const result: Value = blk: {
                        const len: u32 = @intCast(tableinst.entries.len);
                        const new_len, const len_ov = @addWithOverflow(len, count);
                        if (len_ov != 0)
                            break :blk GROW_ERROR_VALUE;
                        if (tableinst.type.limits.max) |max| if (new_len > max)
                            break :blk GROW_ERROR_VALUE;
                        if (store.alloc.realloc(tableinst.entries, new_len)) |new| {
                            tableinst.entries = new;
                            @memset(new[len..], ref);
                            tableinst.type.limits.min = new_len;
                            break :blk .{ .i32 = len };
                        } else |_| {
                            break :blk GROW_ERROR_VALUE;
                        }
                    };
                    self.push(.value, result);
                },
                .@"table.size" => {
                    const tableidx = self.next(.table);
                    const tableinst = self.instance.tables[tableidx.table_index];

                    self.push(.value, .{ .i32 = @intCast(tableinst.entries.len) });
                },
                .@"table.fill" => {
                    const tableidx = self.next(.table);
                    const tableinst = self.instance.tables[tableidx.table_index];

                    const count = self.pop(.value).i32;
                    const fill = self.pop(.value);
                    tableinst.type.assertType(fill);
                    const start = self.pop(.value).i32;

                    const end, const overflow = @addWithOverflow(start, count);
                    if (overflow != 0 or end > tableinst.entries.len)
                        return error.TrapOutOfBounds;
                    @memset(tableinst.entries[start..end], fill);
                },

                .done => {
                    try self.saveScratch(self.stack.items.len - self.activation_link);
                    self.completeFrame();
                    break;
                },

                else => |opcode| std.debug.panic("opcode not implemented {any}", .{opcode}),
            }
        }
        std.mem.reverse(Value, self.scratch.items);
        return self.scratch.items;
    }

    fn gotoLabel(self: *Interpreter, label: usize) void {
        for (0..label + 1) |_| {
            while (self.label_link < self.stack.items.len)
                _ = self.pop(.value);
            self.program_counter = self.pop(.label_continuation);
            self.restore(.label_link);
        }
    }

    fn allocFrame(self: *Interpreter, func: *const Function) !void {
        const growth =
            // Function locals.
            func.locals.len +
            // Activation link save.
            1 +
            // Program counter save.
            1 +
            // Instance save.
            1 +
            // Activation save.
            1 +
            // Label link save.
            1 +
            // Label continuation.
            1 +
            // Max values.
            func.max_values +
            // Max labels.
            func.max_labels * 2;
        const total = self.stack.items.len + growth;
        if (total > self.limits.stack)
            return error.TrapExhaustion;
        try self.stack.ensureTotalCapacity(self.alloc, total);
    }

    fn completeFrame(self: *Interpreter) void {
        var locals_len = self.activation[0].size;
        self.restore(.activation);
        self.restore(.instance);
        self.restore(.program_counter);
        self.restore(.activation_link);
        while (locals_len > 0) : (locals_len -= 1)
            _ = self.pop(.local);
    }

    inline fn getLocal(self: *Interpreter, idx: LocalIndex) *Value {
        return &self.stack.items[self.activation_link - 5 - idx.local_index].local;
    }

    inline fn assertStack(self: *Interpreter, types: []const ValueType) void {
        const start = self.stack.items.len - types.len;
        for (types, self.stack.items[start..]) |exp, act|
            exp.assert(act.value);
    }

    fn next(
        self: *Interpreter,
        comptime tag: std.meta.FieldEnum(Code),
    ) std.meta.FieldType(Code, tag) {
        const result = @field(self.activation[self.program_counter], @tagName(tag));
        trace.emit(.{ .exec = .{ .code = .{
            .pc = self.program_counter,
            .data = @unionInit(trace.TraceEvent.TaggedCode, @tagName(tag), result),
        } } });
        self.program_counter += 1;
        return result;
    }

    fn saveScratch(self: *Interpreter, count: usize) !void {
        try self.scratch.ensureUnusedCapacity(self.alloc, count);
        for (0..count) |_|
            self.scratch.appendAssumeCapacity(self.pop(.value));
    }

    fn pushScratch(self: *Interpreter, comptime tag: std.meta.FieldEnum(Entry)) void {
        while (self.scratch.popOrNull()) |value|
            self.push(tag, value);
    }

    fn save(
        self: *Interpreter,
        comptime tag: std.meta.FieldEnum(Entry),
    ) void {
        self.push(tag, @field(self.*, @tagName(tag)));
    }

    fn restore(
        self: *Interpreter,
        comptime tag: std.meta.FieldEnum(Entry),
    ) void {
        @field(self.*, @tagName(tag)) = self.pop(tag);
    }

    fn push(
        self: *Interpreter,
        comptime tag: std.meta.FieldEnum(Entry),
        payload: std.meta.TagPayloadByName(Entry, @tagName(tag)),
    ) void {
        if (tag == .value) {
            trace.emit(.{ .exec = .{ .push = .{
                .height = self.stack.items.len,
                .value = payload,
            } } });
        }
        self.stack.appendAssumeCapacity(@unionInit(Entry, @tagName(tag), payload));
    }

    fn pop(
        self: *Interpreter,
        comptime tag: std.meta.FieldEnum(Entry),
    ) std.meta.TagPayloadByName(Entry, @tagName(tag)) {
        const payload = @field(self.stack.pop(), @tagName(tag));
        if (tag == .value) {
            trace.emit(.{ .exec = .{ .pop = .{
                .height = self.stack.items.len,
                .value = payload,
            } } });
        }
        return payload;
    }
};

pub const HostFunction = fn (*anyopaque, *Stack) Trap!void;

/// An opaque reference which permits extracting parameters and pushing results.
///
/// This is used within host-implemented functions to work with the runtime's stack.
pub const Stack = opaque {
    const Self = @This();

    pub fn parameter(self: *Self) Value {
        const instance: *Interpreter = @alignCast(@ptrCast(self));
        return instance.pop(.value);
    }

    pub fn result(self: *Self, value: Value) void {
        const instance: *Interpreter = @alignCast(@ptrCast(self));
        instance.push(.value, value);
    }
};

/// The set of possible traps produced while interpretting.
pub const Trap = error{
    TrapUnreachable,
    TrapDivisionByZero,
    TrapOverflow,
    TrapOutOfBounds,
    TrapNullDereference,
    TrapTypeMismatch,
    TrapExhaustion,
    TrapHost,
};

/// The set of possible errors produced while interpretting.
pub const EvalError = std.mem.Allocator.Error || Trap;

const Entry = union {
    activation_link: usize,
    activation: [*]const Code,
    instance: *const Instance,
    program_counter: usize,
    label_link: usize,
    label_continuation: usize,
    local: Value,
    value: Value,
};
