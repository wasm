const std = @import("std");

pub const options = @import("./options.zig");

pub const FunctionIndex = @import("./module.zig").FunctionIndex;
pub const FunctionType = @import("./module.zig").FunctionType;
pub const Module = @import("./module.zig").Module;
pub const ValueType = @import("./module.zig").ValueType;

pub usingnamespace @import("./instance.zig");
pub usingnamespace @import("./trace.zig");

pub const load = @import("./module.zig").load;
