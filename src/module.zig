const std = @import("std");

const leb128 = @import("./leb128.zig");
const trace = @import("./trace.zig");

const Value = @import("./instance.zig").Value;

pub const Module = struct {
    types: []const FunctionType,
    imports: []const Import,
    funcs: []const Function,
    tables: []const Table,
    memories: []const MemoryType,
    globals: []const Global,
    exports: []const Export,
    start: ?FunctionIndex,
    elements: []const ElementSegment,
    data: []const DataSegment,
    // Non-specification defined fields which are used internally.
    imported_functions: usize,
    imported_tables: usize,
    imported_memories: usize,
    imported_globals: usize,

    pub fn lookup(self: Module, name: []const u8) ?ExportDescription {
        return for (self.exports) |exp| {
            if (std.mem.eql(u8, exp.name, name))
                break exp.desc;
        } else null;
    }
};

/// Ref: WebAssembly Specification Release 2.0, Section 5.5.2
const SectionId = enum(u8) {
    custom = 0,
    type = 1,
    import = 2,
    function = 3,
    table = 4,
    memory = 5,
    global = 6,
    @"export" = 7,
    start = 8,
    element = 9,
    code = 10,
    data = 11,
    data_count = 12,
};

/// A single entry in an expression.
pub const Code = union {
    opcode: Opcode,
    vec: usize,
    value: Value,
    size: usize,
    arity: usize,
    reftype: ReferenceType,
    label: LabelIndex,
    function: FunctionIndex,
    type: TypeIndex,
    table: TableIndex,
    local: LocalIndex,
    global: GlobalIndex,
    element: ElementIndex,
    data: DataIndex,
};

/// An operation which may be executed by this interpretter.
///
/// This does not directly correlate to a WASM opcode though the same names are used where it is
/// appropriate.
pub const Opcode = enum(u32) {
    @"unreachable",
    nop,
    block,
    loop,
    @"if",
    @"else",
    end,
    br,
    br_if,
    br_table,
    @"return",
    call,
    call_indirect,
    drop,
    select,
    @"local.get",
    @"local.set",
    @"local.tee",
    @"global.get",
    @"global.set",
    @"table.get",
    @"table.set",
    @"i32.load",
    @"i64.load",
    @"f32.load",
    @"f64.load",
    @"i32.load8_s",
    @"i32.load8_u",
    @"i32.load16_s",
    @"i32.load16_u",
    @"i64.load8_s",
    @"i64.load8_u",
    @"i64.load16_s",
    @"i64.load16_u",
    @"i64.load32_s",
    @"i64.load32_u",
    @"i32.store",
    @"i64.store",
    @"f32.store",
    @"f64.store",
    @"i32.store8",
    @"i32.store16",
    @"i64.store8",
    @"i64.store16",
    @"i64.store32",
    @"memory.size",
    @"memory.grow",
    @"i32.const",
    @"i64.const",
    @"f32.const",
    @"f64.const",
    @"i32.eqz",
    @"i32.eq",
    @"i32.ne",
    @"i32.lt_s",
    @"i32.lt_u",
    @"i32.gt_s",
    @"i32.gt_u",
    @"i32.le_s",
    @"i32.le_u",
    @"i32.ge_s",
    @"i32.ge_u",
    @"i64.eqz",
    @"i64.eq",
    @"i64.ne",
    @"i64.lt_s",
    @"i64.lt_u",
    @"i64.gt_s",
    @"i64.gt_u",
    @"i64.le_s",
    @"i64.le_u",
    @"i64.ge_s",
    @"i64.ge_u",
    @"f32.eq",
    @"f32.ne",
    @"f32.lt",
    @"f32.gt",
    @"f32.le",
    @"f32.ge",
    @"f64.eq",
    @"f64.ne",
    @"f64.lt",
    @"f64.gt",
    @"f64.le",
    @"f64.ge",
    @"i32.clz",
    @"i32.ctz",
    @"i32.popcnt",
    @"i32.add",
    @"i32.sub",
    @"i32.mul",
    @"i32.div_s",
    @"i32.div_u",
    @"i32.rem_s",
    @"i32.rem_u",
    @"i32.and",
    @"i32.or",
    @"i32.xor",
    @"i32.shl",
    @"i32.shr_s",
    @"i32.shr_u",
    @"i32.rotl",
    @"i32.rotr",
    @"i64.clz",
    @"i64.ctz",
    @"i64.popcnt",
    @"i64.add",
    @"i64.sub",
    @"i64.mul",
    @"i64.div_s",
    @"i64.div_u",
    @"i64.rem_s",
    @"i64.rem_u",
    @"i64.and",
    @"i64.or",
    @"i64.xor",
    @"i64.shl",
    @"i64.shr_s",
    @"i64.shr_u",
    @"i64.rotl",
    @"i64.rotr",
    @"f32.abs",
    @"f32.neg",
    @"f32.ceil",
    @"f32.floor",
    @"f32.trunc",
    @"f32.nearest",
    @"f32.sqrt",
    @"f32.add",
    @"f32.sub",
    @"f32.mul",
    @"f32.div",
    @"f32.min",
    @"f32.max",
    @"f32.copysign",
    @"f64.abs",
    @"f64.neg",
    @"f64.ceil",
    @"f64.floor",
    @"f64.trunc",
    @"f64.nearest",
    @"f64.sqrt",
    @"f64.add",
    @"f64.sub",
    @"f64.mul",
    @"f64.div",
    @"f64.min",
    @"f64.max",
    @"f64.copysign",
    @"i32.wrap_i64",
    @"i32.trunc_f32_s",
    @"i32.trunc_f32_u",
    @"i32.trunc_f64_s",
    @"i32.trunc_f64_u",
    @"i64.extend_i32_s",
    @"i64.extend_i32_u",
    @"i64.trunc_f32_s",
    @"i64.trunc_f32_u",
    @"i64.trunc_f64_s",
    @"i64.trunc_f64_u",
    @"f32.convert_i32_s",
    @"f32.convert_i32_u",
    @"f32.convert_i64_s",
    @"f32.convert_i64_u",
    @"f32.demote_f64",
    @"f64.convert_i32_s",
    @"f64.convert_i32_u",
    @"f64.convert_i64_s",
    @"f64.convert_i64_u",
    @"f64.promote_f32",
    @"i32.reinterpret_f32",
    @"i64.reinterpret_f64",
    @"f32.reinterpret_i32",
    @"f64.reinterpret_i64",
    @"i32.extend8_s",
    @"i32.extend16_s",
    @"i64.extend8_s",
    @"i64.extend16_s",
    @"i64.extend32_s",

    @"ref.null",
    @"ref.is_null",
    @"ref.func",

    @"i32.trunc_sat_f32_s",
    @"i32.trunc_sat_f32_u",
    @"i32.trunc_sat_f64_s",
    @"i32.trunc_sat_f64_u",
    @"i64.trunc_sat_f32_s",
    @"i64.trunc_sat_f32_u",
    @"i64.trunc_sat_f64_s",
    @"i64.trunc_sat_f64_u",
    @"memory.init",
    @"data.drop",
    @"memory.copy",
    @"memory.fill",

    @"table.init",
    @"elem.drop",
    @"table.copy",
    @"table.grow",
    @"table.size",
    @"table.fill",

    done,

    _,
};

// The following are newtype wrappers over indices into one of the fields above. They have been
// written such that a compile error should be generated when attempting to use the index in the
// wrong context even when the type is not explicit. Zig's duck typing would otherwise make type
// confusion easy, if the fields were not distinctly named.
/// A newtype wrapper for a type index.
pub const TypeIndex = packed struct(u32) { type_index: u32 };
/// A newtype wrapper for a function index.
pub const FunctionIndex = packed struct(u32) { func_index: u32 };
/// A newtype wrapper for a table index.
pub const TableIndex = packed struct(u32) { table_index: u32 };
/// A newtype wrapper for a memory index.
pub const MemoryIndex = packed struct(u32) { mem_index: u32 };
/// A newtype wrapper for a global index.
pub const GlobalIndex = packed struct(u32) { global_index: u32 };
/// A newtype wrapper for a element segment index.
pub const ElementIndex = packed struct(u32) { elem_index: u32 };
/// A newtype wrapper for a data segment index.
pub const DataIndex = packed struct(u32) { data_index: u32 };
/// A newtype wrapper for a local index.
pub const LocalIndex = packed struct(u32) { local_index: u32 };
/// A newtype wrapper for a label index.
pub const LabelIndex = packed struct(u32) { label_index: u32 };

/// A constant initializer expression.
pub const Constant = union(enum) {
    import: GlobalIndex,
    i32: u32,
    i64: u64,
    f32: f32,
    f64: f64,
    v128: [16]u8,
    externref: ?*anyopaque,
    funcref: ?FunctionIndex,
};

/// Ref: WebAssembly Specification Release 2.0, Section 2.3.4
/// Ref: WebAssembly Specification Release 2.0, Section 5.3.4
pub const ValueType = enum(u8) {
    i32 = @intFromEnum(NumberType.i32),
    i64 = @intFromEnum(NumberType.i64),
    f32 = @intFromEnum(NumberType.f32),
    f64 = @intFromEnum(NumberType.f64),
    v128 = @intFromEnum(VectorType.v128),
    externref = @intFromEnum(ReferenceType.externref),
    funcref = @intFromEnum(ReferenceType.funcref),

    pub fn assert(self: ValueType, value: Value) void {
        if (std.debug.runtime_safety) {
            // Invoke runtime safety for accessing a union field. This does not require `Value` to
            // be a tagged union.
            switch (self) {
                inline else => |tag| {
                    _ = @field(value, @tagName(tag));
                },
            }
        }
    }

    pub fn kind(self: ValueType) enum { num, vec, ref } {
        return switch (self) {
            .i32, .i64, .f32, .f64 => .num,
            .v128 => .vec,
            .funcref, .externref => .ref,
        };
    }

    pub fn default(self: ValueType) Value {
        return switch (self) {
            .i32 => .{ .i32 = 0 },
            .i64 => .{ .i64 = 0 },
            .f32 => .{ .f32 = 0.0 },
            .f64 => .{ .f64 = 0.0 },
            .v128 => .{ .v128 = [_]u8{0} ** 16 },
            .externref => .{ .externref = null },
            .funcref => .{ .funcref = null },
        };
    }
};

/// Ref: WebAssembly Specification Release 2.0, Section 2.3.1
/// Ref: WebAssembly Specification Release 2.0, Section 5.3.1
pub const NumberType = enum(u8) {
    i32 = 0x7f,
    i64 = 0x7e,
    f32 = 0x7d,
    f64 = 0x7c,

    pub fn toValueType(self: NumberType) ValueType {
        return @enumFromInt(@intFromEnum(self));
    }
};

/// Ref: WebAssembly Specification Release 2.0, Section 2.3.2
/// Ref: WebAssembly Specification Release 2.0, Section 5.3.2
pub const VectorType = enum(u8) {
    v128 = 0x7b,

    pub fn toValueType(self: VectorType) ValueType {
        return @enumFromInt(@intFromEnum(self));
    }
};

/// Ref: WebAssembly Specification Release 2.0, Section 2.3.3
/// Ref: WebAssembly Specification Release 2.0, Section 5.3.3
pub const ReferenceType = enum(u8) {
    funcref = 0x70,
    externref = 0x6f,

    pub fn toValueType(self: ReferenceType) ValueType {
        return @enumFromInt(@intFromEnum(self));
    }
};

/// Ref: WebAssembly Specification Release 2.0, Section 2.3.6
pub const FunctionType = struct {
    parameters: []const ValueType = &.{},
    results: []const ValueType = &.{},

    pub fn matches(self: FunctionType, other: FunctionType) bool {
        if (self.parameters.len != other.parameters.len)
            return false;
        for (self.parameters, other.parameters) |self_parameter, other_parameter|
            if (self_parameter != other_parameter)
                return false;
        if (self.results.len != other.results.len)
            return false;
        for (self.results, other.results) |self_result, other_result|
            if (self_result != other_result)
                return false;
        return true;
    }
};

pub const Import = struct {
    module: []const u8,
    name: []const u8,
    desc: ImportDescription,
};

pub const ImportDescription = union(ExternalType) {
    function: TypeIndex,
    table: Table,
    memory: MemoryType,
    global: GlobalType,
};

pub const Function = struct {
    type: TypeIndex,
    locals: []const ValueType,
    body: []const Code,
    max_values: usize,
    max_labels: usize,
};

pub const Table = struct {
    type: ReferenceType,
    limits: Limits,

    pub fn assertType(self: Table, value: Value) void {
        self.type.toValueType().assert(value);
    }

    pub fn matches(self: Table, other: Table) bool {
        return self.type == other.type and self.limits.matches(other.limits);
    }
};

pub const MemoryType = struct {
    limits: Limits,
};

pub const Limits = struct {
    min: u32,
    max: ?u32,

    pub fn matches(self: Limits, other: Limits) bool {
        // n1 is larger than or equal to n2
        if (self.min < other.min)
            return false;
        const other_max = other.max orelse
            return true;
        const self_max = self.max orelse
            return false;
        return self_max <= other_max;
    }
};

pub const Global = struct {
    type: GlobalType,
    init: Constant,
};

pub const GlobalType = struct {
    type: ValueType,
    mutability: GlobalMutability,

    pub fn matches(self: GlobalType, other: GlobalType) bool {
        return self.type == other.type and self.mutability == other.mutability;
    }
};

pub const GlobalMutability = enum(u1) {
    immutable = 0x00,
    mutable = 0x01,
};

pub const Export = struct {
    name: []const u8,
    desc: ExportDescription,
};

pub const ExportDescription = union(ExternalType) {
    function: FunctionIndex,
    table: TableIndex,
    memory: MemoryIndex,
    global: GlobalIndex,
};

pub const ElementSegment = struct {
    type: ReferenceType,
    init: []const Constant,
    mode: union(enum) {
        passive,
        active: struct {
            table: TableIndex,
            offset: Constant,
        },
        declarative,
    },
};

pub const DataSegment = union(enum) {
    passive: []const u8,
    active: Active,

    pub const Active = struct {
        memory: MemoryIndex,
        offset: Constant,
        data: []const u8,
    };
};

pub const ExternalType = enum {
    function,
    table,
    memory,
    global,
};

pub fn load(alloc: std.mem.Allocator, arena: std.mem.Allocator, reader: anytype) !Module {
    var module = Module{
        .types = &.{},
        .imports = &.{},
        .funcs = &.{},
        .tables = &.{},
        .memories = &.{},
        .globals = &.{},
        .exports = &.{},
        .start = null,
        .elements = &.{},
        .data = &.{},
        .imported_functions = 0,
        .imported_tables = 0,
        .imported_memories = 0,
        .imported_globals = 0,
    };
    var data_prealloc: ?[]DataSegment = null;

    var decoder = Decoder{
        .alloc = alloc,
        .module = &module,
    };
    defer decoder.deinit();

    var expected_sections = std.BoundedArray(SectionId, 12).fromSlice(&.{
        .data,
        .code,
        .data_count,
        .element,
        .start,
        .@"export",
        .global,
        .memory,
        .table,
        .function,
        .import,
        .type,
    }) catch unreachable;
    var required_sections = std.EnumSet(SectionId).initEmpty();

    // Read the magic header.
    const header = reader.readBytesNoEof(8) catch |err| return switch (err) {
        error.EndOfStream => error.Malformed,
        else => |e| e,
    };
    if (!std.mem.eql(u8, "\x00\x61\x73\x6d\x01\x00\x00\x00", &header))
        return error.Malformed;

    // Iterate over each section, gracefully exiting on EOF.
    while (reader.readByte()) |section_id_int| {
        // Read the length of the section in bytes.
        const section_byte_len = try leb128.readInt(reader, u32);
        // Recognize the section ID integer.
        const section_id = std.meta.intToEnum(SectionId, section_id_int) catch
            return error.Malformed;
        // The module is malformed if any section besides the custom section repeats or is
        // encountered out of order.
        if (section_id != .custom) {
            while (expected_sections.popOrNull()) |expected_id| {
                if (expected_id == section_id)
                    break;
            } else {
                return error.Malformed;
            }
        }
        // Limit further reads within the section to that length.
        var section_stream = std.io.limitedReader(reader, section_byte_len);
        const section_reader = section_stream.reader();
        // Read each section.
        switch (section_id) {
            .custom => {
                // TODO: Do something with custom sections. This presently just validates the
                // section.
                _ = try readName(section_reader, arena);
                section_reader.skipBytes(section_stream.bytes_left, .{}) catch |err| {
                    return switch (err) {
                        error.EndOfStream => error.Malformed,
                        else => |e| e,
                    };
                };
            },
            .type => {
                const len = try leb128.readInt(section_reader, u32);
                std.debug.assert(module.types.len == 0);
                const types = try arena.alloc(FunctionType, len);
                for (types) |*slot| {
                    const magic_number = section_reader.readByte() catch |err| return switch (err) {
                        error.EndOfStream => error.Malformed,
                        else => |e| e,
                    };
                    if (magic_number != 0x60)
                        return error.Malformed;

                    const parameters_len = try leb128.readInt(section_reader, u32);
                    const parameters = try arena.alloc(ValueType, parameters_len);
                    for (parameters) |*param|
                        param.* = try readByteEnum(section_reader, ValueType);
                    slot.parameters = parameters;

                    const results_len = try leb128.readInt(section_reader, u32);
                    const results = try arena.alloc(ValueType, results_len);
                    for (results) |*result|
                        result.* = try readByteEnum(section_reader, ValueType);
                    slot.results = results;
                }
                module.types = types;
            },
            .import => {
                var imported_functions = std.ArrayListUnmanaged(TypeIndex){};
                defer imported_functions.deinit(alloc);
                var imported_tables = std.ArrayListUnmanaged(ReferenceType){};
                defer imported_tables.deinit(alloc);
                var imported_globals = std.ArrayListUnmanaged(GlobalType){};
                defer imported_globals.deinit(alloc);

                const len = try leb128.readInt(section_reader, u32);
                const imports = try arena.alloc(Import, len);
                for (imports) |*slot| {
                    slot.module = try readName(section_reader, arena);
                    slot.name = try readName(section_reader, arena);
                    const tag = try readByteEnum(section_reader, std.meta.Tag(ImportDescription));
                    slot.desc = switch (tag) {
                        .function => blk: {
                            const typeidx = try decoder.readIndex(.type, section_reader);
                            // This function index is created bypassing the `Decoder.index` method
                            // because the function is being created here.
                            try decoder.permitFunctionReference(.{ .func_index = @intCast(
                                imported_functions.items.len,
                            ) });
                            try imported_functions.append(alloc, typeidx);
                            break :blk .{ .function = typeidx };
                        },
                        .table => blk: {
                            const table_type = try readTableType(section_reader);
                            try imported_tables.append(alloc, table_type.type);
                            break :blk .{ .table = table_type };
                        },
                        .memory => blk: {
                            module.imported_memories += 1;
                            break :blk .{ .memory = .{ .limits = try readLimits(section_reader) } };
                        },
                        .global => blk: {
                            const valtype = try readByteEnum(section_reader, ValueType);
                            const mutability = try readByteEnum(section_reader, GlobalMutability);
                            const global_type = .{
                                .type = valtype,
                                .mutability = mutability,
                            };
                            try imported_globals.append(alloc, global_type);
                            break :blk .{ .global = global_type };
                        },
                    };
                }
                module.imports = imports;
                module.imported_functions = imported_functions.items.len;
                decoder.imported_functions = try imported_functions.toOwnedSlice(alloc);
                module.imported_tables = imported_tables.items.len;
                decoder.imported_tables = try imported_tables.toOwnedSlice(alloc);
                module.imported_globals = imported_globals.items.len;
                decoder.imported_globals = try imported_globals.toOwnedSlice(alloc);
                if (module.imported_memories > 1)
                    return error.Invalid;
            },
            .function => {
                const len = try leb128.readInt(section_reader, u32);
                std.debug.assert(module.funcs.len == 0);
                const funcs = try arena.alloc(Function, len);
                for (funcs) |*slot|
                    slot.type = try decoder.readIndex(.type, section_reader);
                module.funcs = funcs;
                if (funcs.len > 0)
                    required_sections.insert(.code);
            },
            .table => {
                const len = try leb128.readInt(section_reader, u32);
                std.debug.assert(module.tables.len == 0);
                const tables = try arena.alloc(Table, len);
                for (tables) |*slot|
                    slot.* = try readTableType(section_reader);
                module.tables = tables;
            },
            .global => {
                const len = try leb128.readInt(section_reader, u32);
                std.debug.assert(module.globals.len == 0);
                const globals = try arena.alloc(Global, len);
                for (globals) |*slot| {
                    slot.type.type = try readByteEnum(section_reader, ValueType);
                    slot.type.mutability = try readByteEnum(section_reader, GlobalMutability);
                    slot.init = try decoder.readConstant(section_reader, slot.type.type);
                }
                module.globals = globals;
            },
            .memory => {
                const len = try leb128.readInt(section_reader, u32);
                std.debug.assert(module.memories.len == 0);
                if (len + module.imported_memories > 1)
                    return error.Invalid;
                const memories = try arena.alloc(MemoryType, len);
                for (memories) |*slot| {
                    slot.limits = try readLimits(section_reader);
                    if (slot.limits.min > 1 << 16)
                        return error.Invalid;
                    if (slot.limits.max) |max| if (max > 1 << 16)
                        return error.Invalid;
                }
                module.memories = memories;
            },
            .@"export" => {
                var set = std.StringHashMapUnmanaged(void){};
                defer set.deinit(alloc);
                const len = try leb128.readInt(section_reader, u32);
                std.debug.assert(module.exports.len == 0);
                const exports = try arena.alloc(Export, len);
                for (exports) |*slot| {
                    slot.name = try readName(section_reader, arena);
                    if (set.contains(slot.name))
                        return error.Invalid;
                    try set.put(alloc, slot.name, {});
                    const kind = try section_reader.readByte();
                    const idx = try leb128.readInt(section_reader, u32);
                    slot.desc = switch (kind) {
                        0x00 => blk: {
                            const funcidx = try decoder.index(.func, idx);
                            try decoder.permitFunctionReference(funcidx);
                            break :blk .{ .function = funcidx };
                        },
                        0x01 => .{ .table = try decoder.index(.table, idx) },
                        0x02 => .{ .memory = try decoder.index(.mem, idx) },
                        0x03 => .{ .global = try decoder.index(.global, idx) },
                        else => return error.Malformed,
                    };
                }
                module.exports = exports;
            },
            .start => {
                const funcidx = try leb128.readInt(section_reader, u32);
                const start = try decoder.index(.func, funcidx);
                const typeidx = decoder.getFunctionType(start);
                const starttype = module.types[typeidx.type_index];
                if (starttype.parameters.len > 0 or starttype.results.len > 0)
                    return error.Invalid;
                module.start = start;
            },
            .element => {
                const len = try leb128.readInt(section_reader, u32);
                std.debug.assert(module.elements.len == 0);
                const element_segments = try arena.alloc(ElementSegment, len);
                for (element_segments) |*slot| {
                    const flags = std.bit_set.IntegerBitSet(32){
                        .mask = try leb128.readInt(section_reader, u32),
                    };
                    // If the first bit is set, then the element segment is either passive or
                    // declarative.
                    const is_passive_or_declarative = flags.isSet(0);
                    // The second bit indicates an active element segment has an explicit table
                    // index or indicates a passive element segment.
                    const active_has_tableidx_or_declarative = flags.isSet(1);
                    // Having either the first bit or the second bit set indicates there is an
                    // explicit reference type.
                    const has_type = flags.isSet(0) or active_has_tableidx_or_declarative;
                    // If the third bit is set, then the elements are specified as initialization
                    // constants.
                    const uses_init_constants = flags.isSet(2);

                    const activetype: ?ReferenceType = if (is_passive_or_declarative) blk: {
                        // Passive or declarative element segment.
                        if (active_has_tableidx_or_declarative) {
                            slot.mode = .declarative;
                        } else {
                            slot.mode = .passive;
                        }
                        break :blk null;
                    } else blk: {
                        // Active element segment.
                        const tableidx = try decoder.index(
                            .table,
                            if (active_has_tableidx_or_declarative)
                                try leb128.readInt(section_reader, u32)
                            else
                                0,
                        );
                        const tabletype = decoder.getTableType(tableidx);
                        const offset = try decoder.readConstant(section_reader, .i32);
                        slot.mode = .{ .active = .{
                            .table = tableidx,
                            .offset = offset,
                        } };
                        break :blk tabletype;
                    };
                    if (uses_init_constants) {
                        if (has_type) {
                            slot.type = try readByteEnum(section_reader, ReferenceType);
                        } else {
                            slot.type = .funcref;
                        }
                    } else {
                        if (has_type and try section_reader.readByte() != 0x00)
                            return error.Malformed;
                        slot.type = .funcref;
                    }
                    if (activetype) |t| if (t != slot.type)
                        return error.Invalid;
                    const elements_len = try leb128.readInt(section_reader, u32);
                    const elements = try arena.alloc(Constant, elements_len);
                    if (uses_init_constants) {
                        const valuetype = slot.type.toValueType();
                        // Element initialization constants.
                        for (elements) |*element|
                            element.* = try decoder.readConstant(section_reader, valuetype);
                    } else {
                        // Function indicies.
                        for (elements) |*element| {
                            const funcidx = try decoder.readIndex(.func, section_reader);
                            try decoder.permitFunctionReference(funcidx);
                            element.* = .{ .funcref = funcidx };
                        }
                    }
                    slot.init = elements;
                }
                module.elements = element_segments;
            },
            .data_count => {
                const count = try leb128.readInt(section_reader, u32);
                const data = try arena.alloc(DataSegment, count);
                data_prealloc = data;
                module.data = data;
                if (data.len > 0)
                    required_sections.insert(.data);
            },
            .code => {
                const len = try leb128.readInt(section_reader, u32);
                if (len != module.funcs.len)
                    return error.Invalid;
                for (@constCast(module.funcs)) |*slot| {
                    // Read the length of the expression in bytes.
                    const code_byte_len = try leb128.readInt(section_reader, u32);
                    // Limit further reads within the expression to that length.
                    var code_stream = std.io.limitedReader(section_reader, code_byte_len);
                    const code_reader = code_stream.reader();

                    // Copy the parameters as locals.
                    var locals = std.ArrayListUnmanaged(ValueType){};
                    const function_type = module.types[slot.type.type_index];
                    try locals.appendSlice(arena, function_type.parameters);

                    // Read the compressed locals vector.
                    var compressed_locals_len = try leb128.readInt(code_reader, u32);
                    while (compressed_locals_len > 0) : (compressed_locals_len -= 1) {
                        const type_count = try leb128.readInt(code_reader, u32);
                        const valtype = try readByteEnum(code_reader, ValueType);
                        try locals.appendNTimes(arena, valtype, type_count);
                    }
                    slot.locals = try locals.toOwnedSlice(arena);
                    if (slot.locals.len > std.math.maxInt(u32))
                        return error.Invalid;

                    // Read the function body.
                    defer decoder.results.clearRetainingCapacity();
                    const expr = try decoder.decodeExpression(
                        arena,
                        code_reader,
                        .{ .indirect = slot.type },
                        slot.locals,
                    );
                    slot.body = expr.code;
                    slot.max_values = expr.max_values;
                    slot.max_labels = expr.max_labels;
                }
            },
            .data => {
                const len = try leb128.readInt(section_reader, u32);
                const data = if (data_prealloc) |data| blk: {
                    if (len != data.len)
                        return error.Invalid;
                    break :blk data;
                } else blk: {
                    std.debug.assert(module.data.len == 0);
                    break :blk try arena.alloc(DataSegment, len);
                };
                for (data) |*slot| {
                    const flags = try leb128.readInt(section_reader, u32);
                    if (flags & 1 != 0) {
                        const bytes_len = try leb128.readInt(section_reader, u32);
                        const bytes = try arena.alloc(u8, bytes_len);
                        try section_reader.readNoEof(bytes);
                        slot.* = .{ .passive = bytes };
                    } else {
                        const memidx = try decoder.index(
                            .mem,
                            if (flags & 2 != 0)
                                try leb128.readInt(section_reader, u32)
                            else
                                0,
                        );

                        const constant = try decoder.readConstant(section_reader, .i32);

                        const bytes_len = try leb128.readInt(section_reader, u32);
                        const bytes = try arena.alloc(u8, bytes_len);
                        section_reader.readNoEof(bytes) catch |err| return switch (err) {
                            error.EndOfStream => error.Malformed,
                            else => |e| e,
                        };

                        slot.* = .{ .active = .{
                            .memory = memidx,
                            .offset = constant,
                            .data = bytes,
                        } };
                    }
                }
                module.data = data;
            },
        }
        if (section_stream.bytes_left != 0)
            return error.Malformed;
        required_sections.remove(section_id);
    } else |err| switch (err) {
        error.EndOfStream => {},
        else => |e| return e,
    }

    if (required_sections.count() > 0)
        return error.Invalid;

    return module;
}

const Decoder = struct {
    alloc: std.mem.Allocator,
    module: *const Module,
    imported_functions: []const TypeIndex = &.{},
    imported_tables: []const ReferenceType = &.{},
    imported_globals: []const GlobalType = &.{},
    referenceable_functions: std.AutoHashMapUnmanaged(FunctionIndex, void) = .{},
    values: std.SegmentedList(?ValueType, 64) = .{},
    controls: std.SegmentedList(ControlFrame, 16) = .{},
    input_types: std.SegmentedList(ValueType, 64) = .{},
    output_types: std.SegmentedList(ValueType, 64) = .{},
    size_indices: std.SegmentedList(usize, 32) = .{},
    results: std.ArrayListUnmanaged(Code) = .{},
    max_values: usize = 0,
    max_labels: usize = 0,

    const ControlFrame = struct {
        opcode: Opcode,
        type: BlockType,
        inputs_height: usize,
        inputs_end: usize,
        outputs_height: usize,
        outputs_end: usize,
        values_height: usize,
        @"unreachable": bool,
    };

    pub fn deinit(self: *Decoder) void {
        self.alloc.free(self.imported_functions);
        self.alloc.free(self.imported_tables);
        self.alloc.free(self.imported_globals);
        self.referenceable_functions.deinit(self.alloc);
        self.values.deinit(self.alloc);
        self.controls.deinit(self.alloc);
        self.input_types.deinit(self.alloc);
        self.output_types.deinit(self.alloc);
        self.size_indices.deinit(self.alloc);
        self.results.deinit(self.alloc);
    }

    const Index = union(enum) {
        type: TypeIndex,
        func: FunctionIndex,
        table: TableIndex,
        mem: MemoryIndex,
        global: GlobalIndex,
        elem: ElementIndex,
        data: DataIndex,
        local: LocalIndex,
        label: LabelIndex,
    };

    fn readBlockType(self: *const Decoder, reader: anytype) !BlockType {
        const int = try leb128.readInt(reader, i64);
        return switch (int) {
            leb128.decode("\x40", i64) => .void,
            leb128.decode(std.mem.asBytes(&@intFromEnum(ValueType.i32)), i64) => .{ .direct = .i32 },
            leb128.decode(std.mem.asBytes(&@intFromEnum(ValueType.i64)), i64) => .{ .direct = .i64 },
            leb128.decode(std.mem.asBytes(&@intFromEnum(ValueType.f32)), i64) => .{ .direct = .f32 },
            leb128.decode(std.mem.asBytes(&@intFromEnum(ValueType.f64)), i64) => .{ .direct = .f64 },
            leb128.decode(std.mem.asBytes(&@intFromEnum(ValueType.funcref)), i64) => .{ .direct = .funcref },
            leb128.decode(std.mem.asBytes(&@intFromEnum(ValueType.externref)), i64) => .{ .direct = .externref },
            0...std.math.maxInt(u32) => .{ .indirect = try self.index(.type, @intCast(int)) },
            else => error.Malformed,
        };
    }

    pub fn readIndex(
        self: *Decoder,
        comptime tag: std.meta.Tag(Index),
        reader: anytype,
    ) !std.meta.TagPayload(Index, tag) {
        const idx = try leb128.readInt(reader, u32);
        return try self.index(tag, idx);
    }

    pub fn index(
        self: *const Decoder,
        comptime tag: std.meta.Tag(Index),
        idx: u32,
    ) !std.meta.TagPayload(Index, tag) {
        switch (tag) {
            .type => {
                if (idx >= self.module.types.len)
                    return error.Invalid;
                return .{ .type_index = idx };
            },
            .func => {
                if (idx >= self.module.imported_functions + self.module.funcs.len)
                    return error.Invalid;
                return .{ .func_index = idx };
            },
            .table => {
                if (idx >= self.module.imported_tables + self.module.tables.len)
                    return error.Invalid;
                return .{ .table_index = idx };
            },
            .mem => {
                if (idx >= self.module.imported_memories + self.module.memories.len)
                    return error.Invalid;
                return .{ .mem_index = idx };
            },
            .global => {
                if (idx >= self.module.imported_globals + self.module.globals.len)
                    return error.Invalid;
                return .{ .global_index = idx };
            },
            .elem => {
                if (idx >= self.module.elements.len)
                    return error.Invalid;
                return .{ .elem_index = idx };
            },
            .data => {
                if (idx >= self.module.data.len)
                    return error.Invalid;
                return .{ .data_index = idx };
            },
            .local => @compileError("local indices must be validated using local context"),
            .label => @compileError("label indices must be validated using local context"),
        }
    }

    pub fn permitFunctionReference(self: *Decoder, funcidx: FunctionIndex) !void {
        try self.referenceable_functions.put(self.alloc, funcidx, {});
    }

    pub fn readConstant(self: *Decoder, reader: anytype, typ: ValueType) !Constant {
        const opcode = try reader.readByte();
        const result: Constant = switch (opcode) {
            0x23 => blk: {
                const globalidx = try leb128.readInt(reader, u32);
                if (globalidx >= self.imported_globals.len)
                    return error.Invalid;
                const globaltype = self.imported_globals[globalidx];
                if (globaltype.type != typ)
                    return error.Invalid;
                if (globaltype.mutability == .mutable)
                    return error.Malformed;
                // This is a special case for creating a `GlobalIndex` outside of `Decoder.index`
                // because it is actually more restricted than what `Decoder.index` permits.
                break :blk .{ .import = .{ .global_index = globalidx } };
            },
            0x41 => blk: {
                if (typ != .i32)
                    return error.Invalid;
                break :blk .{ .i32 = @bitCast(try leb128.readInt(reader, i32)) };
            },
            0x42 => blk: {
                if (typ != .i64)
                    return error.Invalid;
                break :blk .{ .i64 = @bitCast(try leb128.readInt(reader, i64)) };
            },
            0x43 => blk: {
                if (typ != .f32)
                    return error.Invalid;
                break :blk .{ .f32 = @as(f32, @bitCast(try reader.readBytesNoEof(4))) };
            },
            0x44 => blk: {
                if (typ != .f64)
                    return error.Invalid;
                break :blk .{ .f64 = @as(f64, @bitCast(try reader.readBytesNoEof(8))) };
            },
            0xd0 => blk: {
                const reftype = try readByteEnum(reader, ReferenceType);
                if (reftype.toValueType() != typ)
                    return error.Invalid;
                break :blk switch (typ) {
                    .externref => .{ .externref = null },
                    .funcref => .{ .funcref = null },
                    else => return error.Invalid,
                };
            },
            0xd2 => blk: {
                if (typ != .funcref)
                    return error.Invalid;
                const funcidx = try self.readIndex(.func, reader);
                try self.permitFunctionReference(funcidx);
                break :blk .{ .funcref = funcidx };
            },
            0x0b => return error.Invalid,
            else => return error.Malformed,
        };
        const end_byte = reader.readByte() catch |err| return switch (err) {
            error.EndOfStream => error.Malformed,
            else => |e| e,
        };
        return switch (end_byte) {
            0x0b => result,
            0x23, 0x41, 0x42, 0x43, 0x44, 0xd0, 0xd2 => error.Invalid,
            else => error.Malformed,
        };
    }

    const DecodedExpression = struct {
        code: []const Code,
        max_values: usize,
        max_labels: usize,
    };

    pub fn decodeExpression(
        self: *Decoder,
        alloc: std.mem.Allocator,
        reader: anytype,
        expr_type: BlockType,
        locals: []const ValueType,
    ) !DecodedExpression {
        defer {
            self.max_values = 0;
            self.max_labels = 0;
            self.values.clearRetainingCapacity();
            self.controls.clearRetainingCapacity();
            self.input_types.clearRetainingCapacity();
            self.output_types.clearRetainingCapacity();
            self.size_indices.clearRetainingCapacity();
            self.results.clearRetainingCapacity();
        }

        try self.emit(.size, locals.len);

        _ = try self.pushControlFrame(.call, expr_type);
        // Correct for the parameters being pushed as values when they are actually locals for a
        // function activation.
        try self.expectBlockTypeParameters(expr_type);

        var instruction: usize = 0;
        while (self.controls.len > 0) : (instruction += 1) {
            const section_id_byte = reader.readByte() catch |err| return switch (err) {
                error.EndOfStream => error.Malformed,
                else => |e| e,
            };
            switch (section_id_byte) {
                0x00 => { // unreachable
                    try self.emit(.opcode, .@"unreachable");
                    self.makeTopFrameUnreachable();
                },
                0x01 => { // nop
                    try self.emit(.opcode, .nop);
                },
                0x02 => { // block
                    const block_type = try self.readBlockType(reader);

                    try self.expectBlockTypeParameters(block_type);
                    const ctrl = try self.pushControlFrame(.block, block_type);

                    try self.emit(.opcode, .block);
                    try self.emit(.arity, self.input_types.len - ctrl.inputs_height);
                    try self.deferSize(self.results.items.len);
                    try self.emit(.size, undefined);
                },
                0x03 => { // loop
                    const block_type = try self.readBlockType(reader);

                    try self.expectBlockTypeParameters(block_type);
                    const ctrl = try self.pushControlFrame(.loop, block_type);

                    try self.emit(.opcode, .loop);
                    try self.emit(.arity, self.input_types.len - ctrl.inputs_height);
                },
                0x04 => { // if
                    const block_type = try self.readBlockType(reader);

                    try self.expectValueType(.i32);
                    try self.expectBlockTypeParameters(block_type);
                    const ctrl = try self.pushControlFrame(.@"if", block_type);

                    try self.emit(.opcode, .@"if");
                    try self.emit(.arity, self.input_types.len - ctrl.inputs_height);
                    try self.deferSize(self.results.items.len);
                    try self.emit(.size, undefined);

                    try self.deferSize(self.results.items.len);
                    try self.emit(.size, undefined);
                },
                0x05 => { // else
                    // This one is weird. The second size offset emitted when decoding the `if`
                    // opcode should be set such that it points just beyond the else instruction.
                    // So, we emit the `else` instruction first then pop the control frame.
                    try self.emit(.opcode, .@"else");
                    const else_size_idx = self.results.items.len;
                    try self.emit(.size, undefined);

                    const if_ctrl = try self.popControlFrame();
                    if (if_ctrl.opcode != .@"if")
                        return error.Invalid;
                    self.applySize();
                    _ = try self.pushControlFrame(.@"else", if_ctrl.type);

                    try self.deferSize(else_size_idx);
                },

                0x0b => { // end
                    const ctrl = try self.popControlFrame();
                    switch (ctrl.opcode) {
                        .@"if" => {
                            try self.pushBlockTypeParameters(ctrl.type);
                            try self.expectBlockTypeResults(ctrl.type);
                            self.applySize();
                            self.applySize();
                        },
                        .@"else" => {
                            self.applySize();
                            self.applySize();
                        },
                        .block => {
                            self.applySize();
                        },
                        else => {},
                    }
                    try self.pushBlockTypeResults(ctrl.type);
                    try self.emit(.opcode, .end);
                },
                0x0c => { // br
                    const label = try leb128.readInt(reader, u32);

                    if (self.controls.len <= label)
                        return error.Invalid;
                    const ctrl = self.controls.at(self.controls.len - label - 1);
                    const start, const end, const types = switch (ctrl.opcode) {
                        .loop => .{ ctrl.inputs_height, ctrl.inputs_end, &self.input_types },
                        else => .{ ctrl.outputs_height, ctrl.outputs_end, &self.output_types },
                    };
                    var idx = end;
                    while (start < idx) {
                        idx -= 1;
                        try self.expectValueType(types.at(idx).*);
                    }
                    self.makeTopFrameUnreachable();

                    try self.emit(.opcode, .br);
                    try self.emit(.arity, end - start);
                    try self.emit(.label, .{ .label_index = label });
                },
                0x0d => { // br_if
                    const label = try leb128.readInt(reader, u32);

                    if (self.controls.len <= label)
                        return error.Invalid;
                    try self.expectValueType(.i32);
                    const ctrl = self.controls.at(self.controls.len - label - 1);
                    const start, const end, const types = switch (ctrl.opcode) {
                        .loop => .{ ctrl.inputs_height, ctrl.inputs_end, &self.input_types },
                        else => .{ ctrl.outputs_height, ctrl.outputs_end, &self.output_types },
                    };
                    var idx = end;
                    while (start < idx) {
                        idx -= 1;
                        try self.expectValueType(types.at(idx).*);
                    }
                    while (idx < end) : (idx += 1)
                        try self.pushValueType(types.at(idx).*);

                    try self.emit(.opcode, .br_if);
                    try self.emit(.arity, end - start);
                    try self.emit(.label, .{ .label_index = label });
                },
                0x0e => { // br_table
                    const len = try leb128.readInt(reader, u32);

                    // Validation and emitting is interwoven because it is streaming.
                    try self.expectValueType(.i32);
                    try self.emit(.opcode, .br_table);
                    const arity_idx = self.results.items.len;
                    try self.emit(.arity, undefined);
                    const size_idx = self.results.items.len;
                    try self.emit(.size, undefined);
                    var consistent_arity: usize = undefined;
                    var scratch = std.SegmentedList(?ValueType, 64){};
                    defer scratch.deinit(self.alloc);
                    for (0..len) |table_idx| {
                        const label = try leb128.readInt(reader, u32);
                        if (self.controls.len <= label)
                            return error.Invalid;
                        const ctrl = self.controls.at(self.controls.len - label - 1);
                        const start, const end, const types = switch (ctrl.opcode) {
                            .loop => .{ ctrl.inputs_height, ctrl.inputs_end, &self.input_types },
                            else => .{ ctrl.outputs_height, ctrl.outputs_end, &self.output_types },
                        };
                        var type_idx = end;
                        while (start < type_idx) {
                            type_idx -= 1;
                            const popped = try self.popExpectValueType(types.at(type_idx).*);
                            try scratch.append(self.alloc, popped);
                        }
                        while (scratch.pop()) |popped|
                            try self.pushValueType(popped);
                        const arity = end - start;
                        if (table_idx == 0) {
                            consistent_arity = arity;
                        } else if (consistent_arity != arity) {
                            return error.Invalid;
                        }
                        try self.emit(.label, .{ .label_index = label });
                    }
                    self.results.items[size_idx].size = self.results.items.len - size_idx - 1;
                    const label = try leb128.readInt(reader, u32);
                    if (self.controls.len <= label)
                        return error.Invalid;
                    const ctrl = self.controls.at(self.controls.len - label - 1);
                    const start, const end, const types = switch (ctrl.opcode) {
                        .loop => .{ ctrl.inputs_height, ctrl.inputs_end, &self.input_types },
                        else => .{ ctrl.outputs_height, ctrl.outputs_end, &self.output_types },
                    };
                    var type_idx = end;
                    while (start < type_idx) {
                        type_idx -= 1;
                        try self.expectValueType(types.at(type_idx).*);
                    }
                    const arity = end - start;
                    if (len > 0 and consistent_arity != arity)
                        return error.Invalid;
                    self.makeTopFrameUnreachable();
                    try self.emit(.label, .{ .label_index = label });
                    self.results.items[arity_idx].arity = arity;
                },
                0x0f => { // return
                    const ctrl = self.controls.at(0);
                    const start, const end, const types = switch (ctrl.opcode) {
                        .loop => .{ ctrl.inputs_height, ctrl.inputs_end, &self.input_types },
                        else => .{ ctrl.outputs_height, ctrl.outputs_end, &self.output_types },
                    };
                    var idx = end;
                    while (start < idx) {
                        idx -= 1;
                        try self.expectValueType(types.at(idx).*);
                    }
                    self.makeTopFrameUnreachable();

                    try self.emit(.opcode, .@"return");
                    try self.emit(.arity, end - start);
                },

                0x10 => { // call
                    const funcidx = try self.readIndex(.func, reader);

                    const typeidx = self.getFunctionType(funcidx);
                    const functype = self.module.types[typeidx.type_index];
                    var param_iter = std.mem.reverseIterator(functype.parameters);
                    while (param_iter.next()) |param|
                        try self.expectValueType(param);
                    for (functype.results) |result|
                        try self.pushValueType(result);

                    try self.emit(.opcode, .call);
                    try self.emit(.function, funcidx);
                },
                0x11 => { // call_indirect
                    const typeidx = try self.readIndex(.type, reader);
                    const tableidx = try self.readIndex(.table, reader);

                    const functype = self.module.types[typeidx.type_index];
                    try self.expectValueType(.i32);
                    var param_iter = std.mem.reverseIterator(functype.parameters);
                    while (param_iter.next()) |param|
                        try self.expectValueType(param);
                    for (functype.results) |result|
                        try self.pushValueType(result);
                    const tabletype = self.getTableType(tableidx);
                    if (tabletype != .funcref)
                        return error.Invalid;

                    try self.emit(.opcode, .call_indirect);
                    try self.emit(.type, typeidx);
                    try self.emit(.table, tableidx);
                },

                0x1a => { // drop
                    _ = try self.popValueType();

                    try self.emit(.opcode, .drop);
                },
                0x1b => { // select
                    try self.expectValueType(.i32);
                    const left = try self.popValueType();
                    const right = try self.popValueType();
                    if (left != right and left != null and right != null)
                        return error.Invalid;
                    const result_type = left orelse right;
                    if (result_type) |t| if (t.kind() == .ref)
                        return error.Invalid;
                    try self.pushValueType(result_type);

                    try self.emit(.opcode, .select);
                },
                0x1c => { // select
                    const len = try leb128.readInt(reader, u32);
                    if (len != 1)
                        return error.Invalid;
                    const valtype = try readByteEnum(reader, ValueType);
                    try self.expectValueType(.i32);
                    try self.expectValueType(valtype);
                    try self.expectValueType(valtype);
                    try self.pushValueType(valtype);

                    try self.emit(.opcode, .select);
                },

                0x20 => { // local.get
                    const localidx = .{ .local_index = try leb128.readInt(reader, u32) };

                    if (localidx.local_index >= locals.len)
                        return error.Invalid;
                    try self.pushValueType(locals[localidx.local_index]);

                    try self.emit(.opcode, .@"local.get");
                    try self.emit(.local, localidx);
                },
                0x21 => { // local.set
                    const localidx = .{ .local_index = try leb128.readInt(reader, u32) };

                    if (localidx.local_index >= locals.len)
                        return error.Invalid;
                    try self.expectValueType(locals[localidx.local_index]);

                    try self.emit(.opcode, .@"local.set");
                    try self.emit(.local, localidx);
                },
                0x22 => { // local.tee
                    const localidx = .{ .local_index = try leb128.readInt(reader, u32) };

                    if (localidx.local_index >= locals.len)
                        return error.Invalid;
                    const localtype = locals[localidx.local_index];
                    try self.expectValueType(localtype);
                    try self.pushValueType(localtype);

                    try self.emit(.opcode, .@"local.tee");
                    try self.emit(.local, localidx);
                },
                0x23 => { // global.get
                    const globalidx = try self.readIndex(.global, reader);

                    const valtype = self.getGlobalType(globalidx).type;
                    try self.pushValueType(valtype);

                    try self.emit(.opcode, .@"global.get");
                    try self.emit(.global, globalidx);
                },
                0x24 => { // global.set
                    const globalidx = try self.readIndex(.global, reader);

                    const global_type = self.getGlobalType(globalidx);
                    try self.expectValueType(global_type.type);
                    if (global_type.mutability != .mutable)
                        return error.Invalid;

                    try self.emit(.opcode, .@"global.set");
                    try self.emit(.global, globalidx);
                },
                0x25 => { // table.get
                    const tableidx = try self.readIndex(.table, reader);

                    const tabletype = self.getTableType(tableidx);
                    try self.expectValueType(.i32);
                    try self.pushValueType(tabletype.toValueType());

                    try self.emit(.opcode, .@"table.get");
                    try self.emit(.table, tableidx);
                },
                0x26 => { // table.set
                    const tableidx = try self.readIndex(.table, reader);

                    const tabletype = self.getTableType(tableidx);
                    try self.expectValueType(tabletype.toValueType());
                    try self.expectValueType(.i32);

                    try self.emit(.opcode, .@"table.set");
                    try self.emit(.table, tableidx);
                },

                // Load opcodes.
                inline 0x28...0x35 => |opcode_int| {
                    const opcode, const output, const T = comptime switch (opcode_int) {
                        0x28 => .{ .@"i32.load", .i32, u32 },
                        0x29 => .{ .@"i64.load", .i64, u64 },
                        0x2a => .{ .@"f32.load", .f32, f32 },
                        0x2b => .{ .@"f64.load", .f64, f64 },
                        0x2c => .{ .@"i32.load8_s", .i32, u8 },
                        0x2d => .{ .@"i32.load8_u", .i32, u8 },
                        0x2e => .{ .@"i32.load16_s", .i32, u16 },
                        0x2f => .{ .@"i32.load16_u", .i32, u16 },
                        0x30 => .{ .@"i64.load8_s", .i64, u8 },
                        0x31 => .{ .@"i64.load8_u", .i64, u8 },
                        0x32 => .{ .@"i64.load16_s", .i64, u16 },
                        0x33 => .{ .@"i64.load16_u", .i64, u16 },
                        0x34 => .{ .@"i64.load32_s", .i64, u32 },
                        0x35 => .{ .@"i64.load32_u", .i64, u32 },
                        else => unreachable,
                    };

                    if (self.module.memories.len + self.module.imported_memories == 0)
                        return error.Invalid;
                    const alignment = try leb128.readInt(reader, u32);
                    const offset = try leb128.readInt(reader, u32);

                    if (alignment > comptime std.math.log2_int(usize, @bitSizeOf(T) / 8))
                        return error.Invalid;
                    try self.expectValueType(.i32);
                    try self.pushValueType(output);

                    try self.emit(.opcode, opcode);
                    try self.emit(.value, .{ .i32 = offset });
                },

                // Store opcodes.
                inline 0x36...0x3e => |opcode_int| {
                    const opcode, const input, const T = comptime switch (opcode_int) {
                        0x36 => .{ .@"i32.store", .i32, u32 },
                        0x37 => .{ .@"i64.store", .i64, u64 },
                        0x38 => .{ .@"f32.store", .f32, f32 },
                        0x39 => .{ .@"f64.store", .f64, f64 },
                        0x3a => .{ .@"i32.store8", .i32, u8 },
                        0x3b => .{ .@"i32.store16", .i32, u16 },
                        0x3c => .{ .@"i64.store8", .i64, u8 },
                        0x3d => .{ .@"i64.store16", .i64, u16 },
                        0x3e => .{ .@"i64.store32", .i64, u32 },
                        else => unreachable,
                    };

                    if (self.module.memories.len + self.module.imported_memories == 0)
                        return error.Invalid;
                    const alignment = try leb128.readInt(reader, u32);
                    const offset = try leb128.readInt(reader, u32);

                    if (alignment > comptime std.math.log2_int(usize, @bitSizeOf(T) / 8))
                        return error.Invalid;
                    try self.expectValueType(input);
                    try self.expectValueType(.i32);

                    try self.emit(.opcode, opcode);
                    try self.emit(.value, .{ .i32 = offset });
                },
                0x3f => { // memory.size
                    const memidx = try self.index(
                        .mem,
                        reader.readByte() catch |err| return switch (err) {
                            error.EndOfStream => error.Malformed,
                            else => |e| e,
                        },
                    );
                    if (memidx.mem_index != 0)
                        return error.Invalid;

                    try self.pushValueType(.i32);

                    try self.emit(.opcode, .@"memory.size");
                },
                0x40 => { // memory.grow
                    const memidx = try self.index(
                        .mem,
                        reader.readByte() catch |err| return switch (err) {
                            error.EndOfStream => error.Malformed,
                            else => |e| e,
                        },
                    );
                    if (memidx.mem_index != 0)
                        return error.Invalid;

                    try self.expectValueType(.i32);
                    try self.pushValueType(.i32);

                    try self.emit(.opcode, .@"memory.grow");
                },
                0x41 => { // i32.const
                    const value = try leb128.readInt(reader, i32);

                    try self.pushValueType(.i32);

                    try self.emit(.opcode, .@"i32.const");
                    try self.emit(.value, .{ .i32 = @bitCast(value) });
                },
                0x42 => { // i64.const
                    const value = try leb128.readInt(reader, i64);

                    try self.pushValueType(.i64);

                    try self.emit(.opcode, .@"i64.const");
                    try self.emit(.value, .{ .i64 = @bitCast(value) });
                },
                0x43 => { // f32.const
                    const float_bytes = try reader.readBytesNoEof(4);
                    const float = std.mem.bytesToValue(f32, &float_bytes);

                    try self.pushValueType(.f32);

                    try self.emit(.opcode, .@"f32.const");
                    try self.emit(.value, .{ .f32 = float });
                },
                0x44 => { // f64.const
                    const float_bytes = try reader.readBytesNoEof(8);
                    const float = std.mem.bytesToValue(f64, &float_bytes);

                    try self.pushValueType(.f64);

                    try self.emit(.opcode, .@"f64.const");
                    try self.emit(.value, .{ .f64 = float });
                },
                0x45 => try self.operator(.@"i32.eqz", &.{.i32}, .i32),
                0x46 => try self.operator(.@"i32.eq", &.{ .i32, .i32 }, .i32),
                0x47 => try self.operator(.@"i32.ne", &.{ .i32, .i32 }, .i32),
                0x48 => try self.operator(.@"i32.lt_s", &.{ .i32, .i32 }, .i32),
                0x49 => try self.operator(.@"i32.lt_u", &.{ .i32, .i32 }, .i32),
                0x4a => try self.operator(.@"i32.gt_s", &.{ .i32, .i32 }, .i32),
                0x4b => try self.operator(.@"i32.gt_u", &.{ .i32, .i32 }, .i32),
                0x4c => try self.operator(.@"i32.le_s", &.{ .i32, .i32 }, .i32),
                0x4d => try self.operator(.@"i32.le_u", &.{ .i32, .i32 }, .i32),
                0x4e => try self.operator(.@"i32.ge_s", &.{ .i32, .i32 }, .i32),
                0x4f => try self.operator(.@"i32.ge_u", &.{ .i32, .i32 }, .i32),
                0x50 => try self.operator(.@"i64.eqz", &.{.i64}, .i32),
                0x51 => try self.operator(.@"i64.eq", &.{ .i64, .i64 }, .i32),
                0x52 => try self.operator(.@"i64.ne", &.{ .i64, .i64 }, .i32),
                0x53 => try self.operator(.@"i64.lt_s", &.{ .i64, .i64 }, .i32),
                0x54 => try self.operator(.@"i64.lt_u", &.{ .i64, .i64 }, .i32),
                0x55 => try self.operator(.@"i64.gt_s", &.{ .i64, .i64 }, .i32),
                0x56 => try self.operator(.@"i64.gt_u", &.{ .i64, .i64 }, .i32),
                0x57 => try self.operator(.@"i64.le_s", &.{ .i64, .i64 }, .i32),
                0x58 => try self.operator(.@"i64.le_u", &.{ .i64, .i64 }, .i32),
                0x59 => try self.operator(.@"i64.ge_s", &.{ .i64, .i64 }, .i32),
                0x5a => try self.operator(.@"i64.ge_u", &.{ .i64, .i64 }, .i32),
                0x5b => try self.operator(.@"f32.eq", &.{ .f32, .f32 }, .i32),
                0x5c => try self.operator(.@"f32.ne", &.{ .f32, .f32 }, .i32),
                0x5d => try self.operator(.@"f32.lt", &.{ .f32, .f32 }, .i32),
                0x5e => try self.operator(.@"f32.gt", &.{ .f32, .f32 }, .i32),
                0x5f => try self.operator(.@"f32.le", &.{ .f32, .f32 }, .i32),
                0x60 => try self.operator(.@"f32.ge", &.{ .f32, .f32 }, .i32),
                0x61 => try self.operator(.@"f64.eq", &.{ .f64, .f64 }, .i32),
                0x62 => try self.operator(.@"f64.ne", &.{ .f64, .f64 }, .i32),
                0x63 => try self.operator(.@"f64.lt", &.{ .f64, .f64 }, .i32),
                0x64 => try self.operator(.@"f64.gt", &.{ .f64, .f64 }, .i32),
                0x65 => try self.operator(.@"f64.le", &.{ .f64, .f64 }, .i32),
                0x66 => try self.operator(.@"f64.ge", &.{ .f64, .f64 }, .i32),
                0x67 => try self.operator(.@"i32.clz", &.{.i32}, .i32),
                0x68 => try self.operator(.@"i32.ctz", &.{.i32}, .i32),
                0x69 => try self.operator(.@"i32.popcnt", &.{.i32}, .i32),
                0x6a => try self.operator(.@"i32.add", &.{ .i32, .i32 }, .i32),
                0x6b => try self.operator(.@"i32.sub", &.{ .i32, .i32 }, .i32),
                0x6c => try self.operator(.@"i32.mul", &.{ .i32, .i32 }, .i32),
                0x6d => try self.operator(.@"i32.div_s", &.{ .i32, .i32 }, .i32),
                0x6e => try self.operator(.@"i32.div_u", &.{ .i32, .i32 }, .i32),
                0x6f => try self.operator(.@"i32.rem_s", &.{ .i32, .i32 }, .i32),
                0x70 => try self.operator(.@"i32.rem_u", &.{ .i32, .i32 }, .i32),
                0x71 => try self.operator(.@"i32.and", &.{ .i32, .i32 }, .i32),
                0x72 => try self.operator(.@"i32.or", &.{ .i32, .i32 }, .i32),
                0x73 => try self.operator(.@"i32.xor", &.{ .i32, .i32 }, .i32),
                0x74 => try self.operator(.@"i32.shl", &.{ .i32, .i32 }, .i32),
                0x75 => try self.operator(.@"i32.shr_s", &.{ .i32, .i32 }, .i32),
                0x76 => try self.operator(.@"i32.shr_u", &.{ .i32, .i32 }, .i32),
                0x77 => try self.operator(.@"i32.rotl", &.{ .i32, .i32 }, .i32),
                0x78 => try self.operator(.@"i32.rotr", &.{ .i32, .i32 }, .i32),
                0x79 => try self.operator(.@"i64.clz", &.{.i64}, .i64),
                0x7a => try self.operator(.@"i64.ctz", &.{.i64}, .i64),
                0x7b => try self.operator(.@"i64.popcnt", &.{.i64}, .i64),
                0x7c => try self.operator(.@"i64.add", &.{ .i64, .i64 }, .i64),
                0x7d => try self.operator(.@"i64.sub", &.{ .i64, .i64 }, .i64),
                0x7e => try self.operator(.@"i64.mul", &.{ .i64, .i64 }, .i64),
                0x7f => try self.operator(.@"i64.div_s", &.{ .i64, .i64 }, .i64),
                0x80 => try self.operator(.@"i64.div_u", &.{ .i64, .i64 }, .i64),
                0x81 => try self.operator(.@"i64.rem_s", &.{ .i64, .i64 }, .i64),
                0x82 => try self.operator(.@"i64.rem_u", &.{ .i64, .i64 }, .i64),
                0x83 => try self.operator(.@"i64.and", &.{ .i64, .i64 }, .i64),
                0x84 => try self.operator(.@"i64.or", &.{ .i64, .i64 }, .i64),
                0x85 => try self.operator(.@"i64.xor", &.{ .i64, .i64 }, .i64),
                0x86 => try self.operator(.@"i64.shl", &.{ .i64, .i64 }, .i64),
                0x87 => try self.operator(.@"i64.shr_s", &.{ .i64, .i64 }, .i64),
                0x88 => try self.operator(.@"i64.shr_u", &.{ .i64, .i64 }, .i64),
                0x89 => try self.operator(.@"i64.rotl", &.{ .i64, .i64 }, .i64),
                0x8a => try self.operator(.@"i64.rotr", &.{ .i64, .i64 }, .i64),
                0x8b => try self.operator(.@"f32.abs", &.{.f32}, .f32),
                0x8c => try self.operator(.@"f32.neg", &.{.f32}, .f32),
                0x8d => try self.operator(.@"f32.ceil", &.{.f32}, .f32),
                0x8e => try self.operator(.@"f32.floor", &.{.f32}, .f32),
                0x8f => try self.operator(.@"f32.trunc", &.{.f32}, .f32),
                0x90 => try self.operator(.@"f32.nearest", &.{.f32}, .f32),
                0x91 => try self.operator(.@"f32.sqrt", &.{.f32}, .f32),
                0x92 => try self.operator(.@"f32.add", &.{ .f32, .f32 }, .f32),
                0x93 => try self.operator(.@"f32.sub", &.{ .f32, .f32 }, .f32),
                0x94 => try self.operator(.@"f32.mul", &.{ .f32, .f32 }, .f32),
                0x95 => try self.operator(.@"f32.div", &.{ .f32, .f32 }, .f32),
                0x96 => try self.operator(.@"f32.min", &.{ .f32, .f32 }, .f32),
                0x97 => try self.operator(.@"f32.max", &.{ .f32, .f32 }, .f32),
                0x98 => try self.operator(.@"f32.copysign", &.{ .f32, .f32 }, .f32),
                0x99 => try self.operator(.@"f64.abs", &.{.f64}, .f64),
                0x9a => try self.operator(.@"f64.neg", &.{.f64}, .f64),
                0x9b => try self.operator(.@"f64.ceil", &.{.f64}, .f64),
                0x9c => try self.operator(.@"f64.floor", &.{.f64}, .f64),
                0x9d => try self.operator(.@"f64.trunc", &.{.f64}, .f64),
                0x9e => try self.operator(.@"f64.nearest", &.{.f64}, .f64),
                0x9f => try self.operator(.@"f64.sqrt", &.{.f64}, .f64),
                0xa0 => try self.operator(.@"f64.add", &.{ .f64, .f64 }, .f64),
                0xa1 => try self.operator(.@"f64.sub", &.{ .f64, .f64 }, .f64),
                0xa2 => try self.operator(.@"f64.mul", &.{ .f64, .f64 }, .f64),
                0xa3 => try self.operator(.@"f64.div", &.{ .f64, .f64 }, .f64),
                0xa4 => try self.operator(.@"f64.min", &.{ .f64, .f64 }, .f64),
                0xa5 => try self.operator(.@"f64.max", &.{ .f64, .f64 }, .f64),
                0xa6 => try self.operator(.@"f64.copysign", &.{ .f64, .f64 }, .f64),
                0xa7 => try self.operator(.@"i32.wrap_i64", &.{.i64}, .i32),
                0xa8 => try self.operator(.@"i32.trunc_f32_s", &.{.f32}, .i32),
                0xa9 => try self.operator(.@"i32.trunc_f32_u", &.{.f32}, .i32),
                0xaa => try self.operator(.@"i32.trunc_f64_s", &.{.f64}, .i32),
                0xab => try self.operator(.@"i32.trunc_f64_u", &.{.f64}, .i32),
                0xac => try self.operator(.@"i64.extend_i32_s", &.{.i32}, .i64),
                0xad => try self.operator(.@"i64.extend_i32_u", &.{.i32}, .i64),
                0xae => try self.operator(.@"i64.trunc_f32_s", &.{.f32}, .i64),
                0xaf => try self.operator(.@"i64.trunc_f32_u", &.{.f32}, .i64),
                0xb0 => try self.operator(.@"i64.trunc_f64_s", &.{.f64}, .i64),
                0xb1 => try self.operator(.@"i64.trunc_f64_u", &.{.f64}, .i64),
                0xb2 => try self.operator(.@"f32.convert_i32_s", &.{.i32}, .f32),
                0xb3 => try self.operator(.@"f32.convert_i32_u", &.{.i32}, .f32),
                0xb4 => try self.operator(.@"f32.convert_i64_s", &.{.i64}, .f32),
                0xb5 => try self.operator(.@"f32.convert_i64_u", &.{.i64}, .f32),
                0xb6 => try self.operator(.@"f32.demote_f64", &.{.f64}, .f32),
                0xb7 => try self.operator(.@"f64.convert_i32_s", &.{.i32}, .f64),
                0xb8 => try self.operator(.@"f64.convert_i32_u", &.{.i32}, .f64),
                0xb9 => try self.operator(.@"f64.convert_i64_s", &.{.i64}, .f64),
                0xba => try self.operator(.@"f64.convert_i64_u", &.{.i64}, .f64),
                0xbb => try self.operator(.@"f64.promote_f32", &.{.f32}, .f64),
                0xbc => try self.operator(.@"i32.reinterpret_f32", &.{.f32}, .i32),
                0xbd => try self.operator(.@"i64.reinterpret_f64", &.{.f64}, .i64),
                0xbe => try self.operator(.@"f32.reinterpret_i32", &.{.i32}, .f32),
                0xbf => try self.operator(.@"f64.reinterpret_i64", &.{.i64}, .f64),
                0xc0 => try self.operator(.@"i32.extend8_s", &.{.i32}, .i32),
                0xc1 => try self.operator(.@"i32.extend16_s", &.{.i32}, .i32),
                0xc2 => try self.operator(.@"i64.extend8_s", &.{.i64}, .i64),
                0xc3 => try self.operator(.@"i64.extend16_s", &.{.i64}, .i64),
                0xc4 => try self.operator(.@"i64.extend32_s", &.{.i64}, .i64),

                0xd0 => { // ref.null
                    const valtype = try readByteEnum(reader, ValueType);
                    if (valtype.kind() != .ref)
                        return error.Invalid;

                    try self.pushValueType(valtype);

                    try self.emit(.opcode, .@"ref.null");
                    try self.emit(.value, switch (valtype) {
                        .externref => .{ .externref = null },
                        .funcref => .{ .funcref = null },
                        else => unreachable,
                    });
                },
                0xd1 => { // ref.is_null
                    const valtype = try self.popValueType();
                    if (valtype) |t| if (t.kind() != .ref)
                        return error.Invalid;
                    try self.pushValueType(.i32);

                    try self.emit(.opcode, .@"ref.is_null");
                    const reftype: ReferenceType = if (valtype) |t|
                        @enumFromInt(@intFromEnum(t))
                    else
                        // TODO: Is this ever accessible while interpretting? I think it is only
                        // possible if this instruction is unreachable.
                        .externref;
                    try self.emit(.reftype, reftype);
                },
                0xd2 => { // ref.func
                    // This creates a function index because it is validated more restrictively than
                    // `Decoder.index`.
                    const funcidx = .{ .func_index = try leb128.readInt(reader, u32) };

                    if (self.referenceable_functions.get(funcidx) == null)
                        return error.Invalid;
                    try self.pushValueType(.funcref);

                    try self.emit(.opcode, .@"ref.func");
                    try self.emit(.function, funcidx);
                },

                0xfc => switch (try leb128.readInt(reader, u32)) {
                    0 => try self.operator(.@"i32.trunc_sat_f32_s", &.{.f32}, .i32),
                    1 => try self.operator(.@"i32.trunc_sat_f32_u", &.{.f32}, .i32),
                    2 => try self.operator(.@"i32.trunc_sat_f64_s", &.{.f64}, .i32),
                    3 => try self.operator(.@"i32.trunc_sat_f64_u", &.{.f64}, .i32),
                    4 => try self.operator(.@"i64.trunc_sat_f32_s", &.{.f32}, .i64),
                    5 => try self.operator(.@"i64.trunc_sat_f32_u", &.{.f32}, .i64),
                    6 => try self.operator(.@"i64.trunc_sat_f64_s", &.{.f64}, .i64),
                    7 => try self.operator(.@"i64.trunc_sat_f64_u", &.{.f64}, .i64),
                    8 => { // memory.init
                        const dataidx = try self.readIndex(.data, reader);
                        const memidx = try self.index(
                            .mem,
                            reader.readByte() catch |err| return switch (err) {
                                error.EndOfStream => error.Malformed,
                                else => |e| e,
                            },
                        );
                        if (memidx.mem_index != 0)
                            return error.Invalid;

                        try self.expectValueType(.i32);
                        try self.expectValueType(.i32);
                        try self.expectValueType(.i32);

                        try self.emit(.opcode, .@"memory.init");
                        try self.emit(.data, dataidx);
                    },
                    9 => { // data.drop
                        const dataidx = try self.readIndex(.data, reader);

                        try self.emit(.opcode, .@"data.drop");
                        try self.emit(.data, dataidx);
                    },
                    10 => { // memory.copy
                        const src_memidx = try self.index(
                            .mem,
                            reader.readByte() catch |err| return switch (err) {
                                error.EndOfStream => error.Malformed,
                                else => |e| e,
                            },
                        );
                        if (src_memidx.mem_index != 0)
                            return error.Invalid;
                        const dest_memidx = try self.index(
                            .mem,
                            reader.readByte() catch |err| return switch (err) {
                                error.EndOfStream => error.Malformed,
                                else => |e| e,
                            },
                        );
                        if (dest_memidx.mem_index != 0)
                            return error.Invalid;

                        try self.expectValueType(.i32);
                        try self.expectValueType(.i32);
                        try self.expectValueType(.i32);

                        try self.emit(.opcode, .@"memory.copy");
                    },
                    11 => { // memory.fill
                        const memidx = try self.index(
                            .mem,
                            reader.readByte() catch |err| return switch (err) {
                                error.EndOfStream => error.Malformed,
                                else => |e| e,
                            },
                        );
                        if (memidx.mem_index != 0)
                            return error.Invalid;

                        try self.expectValueType(.i32);
                        try self.expectValueType(.i32);
                        try self.expectValueType(.i32);

                        try self.emit(.opcode, .@"memory.fill");
                    },
                    12 => { // table.init
                        const elemidx = try self.readIndex(.elem, reader);
                        const tableidx = try self.readIndex(.table, reader);

                        const tabletype = self.getTableType(tableidx);
                        const elemtype = self.module.elements[elemidx.elem_index].type;
                        if (tabletype != elemtype)
                            return error.Invalid;
                        try self.expectValueType(.i32);
                        try self.expectValueType(.i32);
                        try self.expectValueType(.i32);

                        try self.emit(.opcode, .@"table.init");
                        try self.emit(.element, elemidx);
                        try self.emit(.table, tableidx);
                    },
                    13 => { // elem.drop
                        const elemidx = try self.readIndex(.elem, reader);

                        try self.emit(.opcode, .@"elem.drop");
                        try self.emit(.element, elemidx);
                    },
                    14 => { // table.copy
                        const left = try self.readIndex(.table, reader);
                        const right = try self.readIndex(.table, reader);

                        const left_type = self.getTableType(left);
                        const right_type = self.getTableType(right);
                        if (left_type != right_type)
                            return error.Invalid;
                        try self.expectValueType(.i32);
                        try self.expectValueType(.i32);
                        try self.expectValueType(.i32);

                        try self.emit(.opcode, .@"table.copy");
                        try self.emit(.table, left);
                        try self.emit(.table, right);
                    },
                    15 => { // table.grow
                        const tableidx = try self.readIndex(.table, reader);

                        const table_type = self.getTableType(tableidx);
                        try self.expectValueType(.i32);
                        try self.expectValueType(table_type.toValueType());
                        try self.pushValueType(.i32);

                        try self.emit(.opcode, .@"table.grow");
                        try self.emit(.table, tableidx);
                    },
                    16 => { // table.size
                        const tableidx = try self.readIndex(.table, reader);

                        try self.pushValueType(.i32);

                        try self.emit(.opcode, .@"table.size");
                        try self.emit(.table, tableidx);
                    },
                    17 => { // table.fill
                        const tableidx = try self.readIndex(.table, reader);

                        const table_type = self.getTableType(tableidx);
                        try self.expectValueType(.i32);
                        try self.expectValueType(table_type.toValueType());
                        try self.expectValueType(.i32);

                        try self.emit(.opcode, .@"table.fill");
                        try self.emit(.table, tableidx);
                    },

                    else => return error.UnsupportedOpcode,
                },

                else => return error.UnsupportedOpcode,
            }
        }
        try self.emit(.opcode, .done);
        const results = try alloc.alloc(Code, self.results.items.len);
        @memcpy(results, self.results.items);
        return .{
            .code = results,
            .max_values = self.max_values,
            .max_labels = self.max_labels,
        };
    }

    fn operator(
        self: *Decoder,
        opcode: Opcode,
        inputs: anytype,
        output: ValueType,
    ) !void {
        inline for (inputs) |input|
            try self.expectValueType(input);
        try self.pushValueType(output);

        try self.emit(.opcode, opcode);
    }

    /// Emit a code entry.
    ///
    /// In addition to being a convenience, this also serves as a trace point.
    fn emit(
        self: *Decoder,
        comptime tag: std.meta.FieldEnum(Code),
        payload: std.meta.FieldType(Code, tag),
    ) !void {
        trace.emit(.{ .comp = .{ .emit = .{
            .pc = self.results.items.len,
            .data = @unionInit(trace.TraceEvent.TaggedCode, @tagName(tag), payload),
        } } });
        try self.results.append(self.alloc, @unionInit(Code, @tagName(tag), payload));
    }

    fn deferSize(self: *Decoder, idx: usize) !void {
        try self.size_indices.append(self.alloc, idx);
    }

    fn applySize(self: *Decoder) void {
        const idx = self.size_indices.pop().?;
        self.results.items[idx] = .{ .size = self.results.items.len - idx };
    }

    fn pushControlFrame(
        self: *Decoder,
        opcode: Opcode,
        block_type: BlockType,
    ) !*ControlFrame {
        const ctrl = try self.controls.addOne(self.alloc);
        ctrl.* = .{
            .opcode = opcode,
            .type = block_type,
            .inputs_height = self.input_types.len,
            .inputs_end = undefined,
            .outputs_height = self.output_types.len,
            .outputs_end = undefined,
            .values_height = self.values.len,
            .@"unreachable" = false,
        };
        switch (block_type) {
            .void => {},
            .direct => |typ| {
                try self.output_types.append(self.alloc, typ);
            },
            .indirect => |idx| {
                const typ = self.module.types[idx.type_index];
                try self.input_types.appendSlice(self.alloc, typ.parameters);
                try self.output_types.appendSlice(self.alloc, typ.results);
                for (typ.parameters) |param|
                    try self.pushValueType(param);
            },
        }
        ctrl.inputs_end = self.input_types.len;
        ctrl.outputs_end = self.output_types.len;
        self.max_labels = @max(self.controls.len, self.max_labels);
        return ctrl;
    }

    fn popControlFrame(self: *Decoder) error{Invalid}!ControlFrame {
        if (self.controls.len == 0)
            return error.Invalid;
        const ctrl_idx = self.controls.len - 1;
        const ctrl = self.controls.at(ctrl_idx);
        while (ctrl.outputs_height < self.output_types.len)
            try self.expectValueType(self.output_types.pop().?);
        if (self.values.len != ctrl.values_height)
            return error.Invalid;
        return self.controls.pop().?;
    }

    fn makeTopFrameUnreachable(self: *Decoder) void {
        const ctrl = self.controls.at(self.controls.len - 1);
        self.values.shrink(ctrl.values_height);
        ctrl.@"unreachable" = true;
    }

    fn pushBlockTypeParameters(self: *Decoder, block_type: BlockType) !void {
        switch (block_type) {
            .void, .direct => {},
            .indirect => |idx| {
                const typ = self.module.types[idx.type_index];
                for (typ.parameters) |param|
                    try self.pushValueType(param);
            },
        }
    }

    fn pushBlockTypeResults(self: *Decoder, block_type: BlockType) !void {
        switch (block_type) {
            .void => {},
            .direct => |typ| {
                try self.pushValueType(typ);
            },
            .indirect => |idx| {
                const typ = self.module.types[idx.type_index];
                for (typ.results) |result|
                    try self.pushValueType(result);
            },
        }
    }

    fn pushValueType(self: *Decoder, value: ?ValueType) !void {
        trace.emit(.{ .comp = .{ .push_value = .{ .height = self.values.len, .data = value } } });
        try self.values.append(self.alloc, value);
        self.max_values = @max(self.values.len, self.max_values);
    }

    fn expectBlockTypeParameters(self: *Decoder, block_type: BlockType) !void {
        switch (block_type) {
            .void, .direct => {},
            .indirect => |idx| {
                const typ = self.module.types[idx.type_index];
                var iter = std.mem.reverseIterator(typ.parameters);
                while (iter.next()) |param|
                    try self.expectValueType(param);
            },
        }
    }

    fn expectBlockTypeResults(self: *Decoder, block_type: BlockType) !void {
        switch (block_type) {
            .void => {},
            .direct => |typ| {
                try self.expectValueType(typ);
            },
            .indirect => |idx| {
                const typ = self.module.types[idx.type_index];
                for (typ.results) |result|
                    try self.expectValueType(result);
            },
        }
    }

    fn expectValueType(self: *Decoder, expected: ?ValueType) !void {
        _ = try self.popExpectValueType(expected);
    }

    fn popExpectValueType(self: *Decoder, expected: ?ValueType) !?ValueType {
        const actual = try self.popValueType();
        if (actual != expected and actual != null and expected != null)
            return error.Invalid;
        return actual;
    }

    fn popValueType(self: *Decoder) error{Invalid}!?ValueType {
        const ctrl = self.controls.at(self.controls.len - 1);
        const popped = if (self.values.len == ctrl.values_height) blk: {
            if (ctrl.@"unreachable")
                break :blk null;
            return error.Invalid;
        } else self.values.pop().?;
        trace.emit(.{ .comp = .{ .pop_value = .{ .height = self.values.len, .data = popped } } });
        return popped;
    }

    inline fn getFunctionType(self: *const Decoder, idx: FunctionIndex) TypeIndex {
        var adjusted: usize = idx.func_index;
        if (adjusted < self.imported_functions.len)
            return self.imported_functions[adjusted];
        adjusted -= self.imported_functions.len;
        return self.module.funcs[adjusted].type;
    }

    inline fn getTableType(self: *const Decoder, idx: TableIndex) ReferenceType {
        var adjusted: usize = idx.table_index;
        if (adjusted < self.imported_tables.len)
            return self.imported_tables[adjusted];
        adjusted -= self.imported_tables.len;
        return self.module.tables[adjusted].type;
    }

    inline fn getGlobalType(self: *const Decoder, idx: GlobalIndex) GlobalType {
        var adjusted: usize = idx.global_index;
        if (adjusted < self.imported_globals.len)
            return self.imported_globals[adjusted];
        adjusted -= self.imported_globals.len;
        return self.module.globals[adjusted].type;
    }
};

const BlockType = union(enum) {
    void,
    direct: ValueType,
    indirect: TypeIndex,
};

fn readTableType(reader: anytype) !Table {
    const reftype = try readByteEnum(reader, ReferenceType);
    const limits = try readLimits(reader);
    return .{
        .type = reftype,
        .limits = limits,
    };
}

fn readLimits(reader: anytype) !Limits {
    const has_max_byte = reader.readByte() catch |err| return switch (err) {
        error.EndOfStream => error.Malformed,
        else => |e| e,
    };
    const has_max = switch (has_max_byte) {
        0x00 => false,
        0x01 => true,
        else => return error.Malformed,
    };
    const min = try leb128.readInt(reader, u32);
    const max = if (has_max) blk: {
        const max = try leb128.readInt(reader, u32);
        if (min > max)
            return error.Invalid;
        break :blk max;
    } else null;
    return .{
        .min = min,
        .max = max,
    };
}

/// Read a length-prefixed, utf8-encoded text string.
///
/// The caller owns the returned slice.
fn readName(reader: anytype, alloc: std.mem.Allocator) ![]const u8 {
    const len = try leb128.readInt(reader, u32);
    const bytes = try alloc.alloc(u8, len);
    reader.readNoEof(bytes) catch |err| return switch (err) {
        error.EndOfStream => error.Malformed,
        else => |e| e,
    };
    if (!std.unicode.utf8ValidateSlice(bytes))
        return error.Invalid;
    return bytes;
}

/// Read an enum, `E`, as a byte integer from `reader`.
fn readByteEnum(reader: anytype, comptime E: type) !E {
    const int = reader.readByte() catch |err| return switch (err) {
        error.EndOfStream => error.Malformed,
        else => |e| e,
    };
    return std.meta.intToEnum(E, int) catch
        error.Malformed;
}
