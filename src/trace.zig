const std = @import("std");

const options = @import("./options.zig");

const Code = @import("./module.zig").Code;
const Opcode = @import("./module.zig").Opcode;
const ValueType = @import("./module.zig").ValueType;

const Value = @import("./instance.zig").Value;

pub fn emit(event: TraceEvent) void {
    if (options.trace) |f|
        f(event);
}

pub const TraceEvent = union(enum) {
    comp: Compilation,
    exec: Execution,

    pub const Compilation = union(enum) {
        emit: struct {
            pc: usize,
            data: TaggedCode,
        },
        push_value: ValueStack,
        pop_value: ValueStack,
        push_control: ControlStack,
        pop_control: ControlStack,

        pub const ValueStack = struct {
            height: usize,
            data: ?ValueType,
        };

        pub const ControlStack = struct {
            height: usize,
            data: Opcode,
        };

        pub fn format(
            self: Compilation,
            comptime fmt: []const u8,
            opts: std.fmt.FormatOptions,
            writer: anytype,
        ) !void {
            _ = fmt;
            _ = opts;
            switch (self) {
                .emit => |code| switch (code.data) {
                    inline else => |payload, tag| {
                        try writer.print("0x{x:0>8} emit {s} {}", .{
                            code.pc,
                            @tagName(tag),
                            payload,
                        });
                    },
                },
                inline .push_value,
                .pop_value,
                .push_control,
                .pop_control,
                => |stack, tag| {
                    try writer.print("{s: >15} {d} {?}", .{
                        @tagName(tag),
                        stack.height,
                        stack.data,
                    });
                },
            }
        }
    };

    pub const Execution = union(enum) {
        code: struct {
            pc: usize,
            data: TaggedCode,
        },
        load: Memory,
        store: Memory,
        copy: struct {
            source: usize,
            destination: usize,
            data: []const u8,
        },
        fill: struct {
            offset: usize,
            length: usize,
            byte: u8,
        },
        push: Stack,
        pop: Stack,

        pub const Memory = struct {
            offset: usize,
            data: []const u8,
        };

        pub const Stack = struct {
            height: usize,
            value: Value,
        };

        pub fn format(
            self: Execution,
            comptime fmt: []const u8,
            opts: std.fmt.FormatOptions,
            writer: anytype,
        ) !void {
            _ = fmt;
            _ = opts;
            switch (self) {
                .code => |code| switch (code.data) {
                    inline .label,
                    .function,
                    .type,
                    .table,
                    .local,
                    .global,
                    => |payload, tag| {
                        try writer.print("0x{x:0>8} code {s} {}", .{
                            code.pc,
                            @tagName(tag),
                            @as(u32, @bitCast(payload)),
                        });
                    },
                    inline else => |payload, tag| {
                        try writer.print("0x{x:0>8} code {s} {}", .{
                            code.pc,
                            @tagName(tag),
                            payload,
                        });
                    },
                },
                inline .load, .store => |mem, dir| try writer.print("{s: >15} [{d}..{d}] {}", .{
                    @tagName(dir),
                    mem.offset,
                    mem.offset + mem.data.len,
                    std.fmt.fmtSliceHexLower(mem.data),
                }),
                .copy => |copy| try writer.print("{s: >15} [{d}..{d}] => [{d}..{d}] {}", .{
                    @tagName(self),
                    copy.source,
                    copy.source + copy.data.len,
                    copy.destination,
                    copy.destination + copy.data.len,
                    std.fmt.fmtSliceHexLower(copy.data),
                }),
                .fill => |fill| try writer.print("{s: >15} [{d}..{d}] 0x{x:0>2}", .{
                    @tagName(self),
                    fill.offset,
                    fill.offset + fill.length,
                    fill.byte,
                }),
                .push, .pop => |stack| {
                    try writer.print("{s: >15} {d} {}", .{
                        @tagName(self),
                        stack.height,
                        stack.value,
                    });
                },
            }
        }
    };

    pub const TaggedCode = Tagged(Code);

    pub fn format(
        self: TraceEvent,
        comptime fmt: []const u8,
        opts: std.fmt.FormatOptions,
        writer: anytype,
    ) !void {
        _ = fmt;
        _ = opts;
        switch (self) {
            inline else => |payload, tag| {
                try writer.print("{s: <4} {}", .{ @tagName(tag), payload });
            },
        }
    }
};

fn Tagged(comptime U: type) type {
    var info = @typeInfo(U);
    info.Union.tag_type = std.meta.FieldEnum(U);
    return @Type(info);
}
