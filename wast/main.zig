const std = @import("std");
const wasm = @import("wasm");

const LOGGER = std.log.scoped(.wast);

pub const wasm_options = struct {
    // pub fn trace(event: wasm.TraceEvent) void {
    //     LOGGER.debug("trace {}", .{event});
    // }
};

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer std.debug.assert(gpa.deinit() == .ok);
    const alloc = gpa.allocator();

    const cwd = std.fs.cwd();

    var args = try std.process.argsWithAllocator(alloc);
    defer args.deinit();
    std.debug.assert(args.skip());

    const source_filename = args.next().?;
    const source = try cwd.readFileAlloc(alloc, source_filename, std.math.maxInt(usize));
    defer alloc.free(source);
    errdefer LOGGER.info("source \"{}\"", .{std.zig.fmtEscapes(source_filename)});

    const json_filename = args.next().?;
    const wast = blk: {
        const file = try cwd.openFile(json_filename, .{});
        defer file.close();
        var stream = std.io.countingReader(file.reader());
        errdefer LOGGER.info("error while decoding json at offset 0x{x:0>4}", .{
            stream.bytes_read,
        });
        var token_source = std.json.reader(alloc, stream.reader());
        defer token_source.deinit();
        break :blk try std.json.parseFromTokenSource(Wast, alloc, &token_source, .{});
    };
    defer wast.deinit();
    errdefer LOGGER.info("json \"{}\"", .{std.zig.fmtEscapes(json_filename)});

    const dir = try cwd.openDir(std.fs.path.dirname(json_filename) orelse ".", .{});

    try wast.value.run(alloc, dir, source);

    std.process.cleanExit();
}

const Wast = struct {
    source_filename: []const u8,
    commands: []const WastCommand,

    const LOAD_ERROR_TEXT_MAP = std.ComptimeStringMap(anyerror, &.{
        .{ "unexpected end", error.Malformed },
        .{ "magic header not detected", error.Malformed },
        .{ "unknown binary version", error.Malformed },
        .{ "malformed section id", error.Malformed },
        .{ "END opcode expected", error.Malformed },
        .{ "unexpected end of section or function", error.Malformed },
        .{ "section size mismatch", error.Malformed },
        .{ "illegal opcode", error.Malformed },
        .{ "zero byte expected", error.Invalid },
        .{ "too many locals", error.Invalid },
        .{ "function and code section have inconsistent lengths", error.Invalid },
        .{ "data count and data section have inconsistent lengths", error.Invalid },
        .{ "data count section required", error.Invalid },
        .{ "malformed reference type", error.Malformed },
        .{ "length out of bounds", error.Malformed },
        .{ "malformed import kind", error.Malformed },
        .{ "unexpected content after last section", error.Malformed },
        .{ "integer representation too long", error.Malformed },
        .{ "integer too large", error.Malformed },
        .{ "unknown global", error.Invalid },
        .{ "unknown global 0", error.Invalid },
        .{ "unknown global 1", error.Invalid },
        .{ "unknown memory", error.Invalid },
        .{ "unknown memory 0", error.Invalid },
        .{ "unknown memory 1", error.Invalid },
        .{ "type mismatch", error.Invalid },
        .{ "constant expression required", error.Malformed },
        .{ "global is immutable", error.Invalid },
        .{ "malformed mutability", error.Malformed },
        .{ "alignment must not be larger than natural", error.Invalid },
        .{ "duplicate export name", error.Invalid },
        .{ "invalid result arity", error.Invalid },
        .{ "malformed UTF-8 encoding", error.Invalid },
        .{ "memory size must be at most 65536 pages (4GiB)", error.Invalid },
        .{ "multiple memories", error.Invalid },
        .{ "size minimum must not be greater than maximum", error.Invalid },
        .{ "start function", error.Invalid },
        .{ "undeclared function reference", error.Invalid },
        .{ "unknown data segment", error.Invalid },
        .{ "unknown data segment 1", error.Invalid },
        .{ "unknown elem segment 0", error.Invalid },
        .{ "unknown elem segment 4", error.Invalid },
        .{ "unknown function", error.Invalid },
        .{ "unknown function 7", error.Invalid },
        .{ "unknown label", error.Invalid },
        .{ "unknown local", error.Invalid },
        .{ "unknown table", error.Invalid },
        .{ "unknown table 0", error.Invalid },
        .{ "unknown type", error.Invalid },
    });

    const TRAP_TEXT_MAP = std.ComptimeStringMap(anyerror, &.{
        .{ "unreachable", error.TrapUnreachable },
        .{ "integer divide by zero", error.TrapDivisionByZero },
        .{ "integer overflow", error.TrapOverflow },
        .{ "invalid conversion to integer", error.TrapOverflow },
        .{ "uninitialized element", error.TrapNullDereference },
        .{ "uninitialized element 2", error.TrapNullDereference },
        .{ "undefined element", error.TrapOutOfBounds },
        .{ "out of bounds memory access", error.TrapOutOfBounds },
        .{ "indirect call type mismatch", error.TrapTypeMismatch },
        .{ "out of bounds table access", error.TrapOutOfBounds },
        .{ "call stack exhausted", error.TrapExhaustion },
    });

    pub fn run(self: Wast, alloc: std.mem.Allocator, dir: std.fs.Dir, source: []const u8) !void {
        _ = source;

        var interpreter = wasm.Interpreter.init(alloc);
        defer interpreter.deinit();

        var arena = std.heap.ArenaAllocator.init(alloc);
        defer arena.deinit();
        const arena_alloc = arena.allocator();

        // TODO: How much memory is consumed in total by never releasing module until the WAST
        // test is done?
        var store = wasm.Store.init(alloc);
        defer store.deinit();
        const import_group = store.importGroup();

        // Initialize the linker with host functions correlating to the deprecated "spectest"
        // module.
        try store.createGlobal(
            import_group,
            "spectest",
            "global_i32",
            &.{ .type = .i32, .mutability = .immutable },
            .{ .i32 = 666 },
        );
        try store.createGlobal(
            import_group,
            "spectest",
            "global_i64",
            &.{ .type = .i64, .mutability = .immutable },
            .{ .i64 = 666 },
        );
        try store.createGlobal(
            import_group,
            "spectest",
            "global_f32",
            &.{ .type = .f32, .mutability = .immutable },
            .{ .f32 = 666.6 },
        );
        try store.createGlobal(
            import_group,
            "spectest",
            "global_f64",
            &.{ .type = .f64, .mutability = .immutable },
            .{ .f64 = 666.6 },
        );
        try store.createTable(
            import_group,
            "spectest",
            "table",
            .{
                .type = .funcref,
                .limits = .{ .min = 10, .max = 20 },
            },
        );
        try store.createMemory(
            import_group,
            "spectest",
            "memory",
            .{ .min = 1, .max = 2 },
        );
        try store.createFunction(
            import_group,
            "spectest",
            "print",
            &.{},
            @constCast(@ptrCast(&{})),
            &spectest.print,
        );
        try store.createFunction(
            import_group,
            "spectest",
            "print_i32",
            &.{ .parameters = &.{.i32} },
            @constCast(@ptrCast(&{})),
            &spectest.print_i32,
        );
        try store.createFunction(
            import_group,
            "spectest",
            "print_i64",
            &.{ .parameters = &.{.i64} },
            @constCast(@ptrCast(&{})),
            &spectest.print_i64,
        );
        try store.createFunction(
            import_group,
            "spectest",
            "print_f32",
            &.{ .parameters = &.{.f32} },
            @constCast(@ptrCast(&{})),
            &spectest.print_f32,
        );
        try store.createFunction(
            import_group,
            "spectest",
            "print_f64",
            &.{ .parameters = &.{.f64} },
            @constCast(@ptrCast(&{})),
            &spectest.print_f64,
        );
        try store.createFunction(
            import_group,
            "spectest",
            "print_i32_f32",
            &.{ .parameters = &.{ .i32, .f32 } },
            @constCast(@ptrCast(&{})),
            &spectest.print_i32_f32,
        );
        try store.createFunction(
            import_group,
            "spectest",
            "print_f64_f64",
            &.{ .parameters = &.{ .f64, .f64 } },
            @constCast(@ptrCast(&{})),
            &spectest.print_f64_f64,
        );

        var named_instances = std.StringHashMapUnmanaged(*wasm.Instance){};
        defer named_instances.deinit(alloc);

        var module: *const wasm.Module = undefined;
        var instance: *wasm.Instance = undefined;

        var maybe_module_filename: ?[]const u8 = null;
        errdefer if (maybe_module_filename) |module_filename|
            LOGGER.info("module \"{}\"", .{std.zig.fmtEscapes(module_filename)});

        var warned_skipping = std.BufSet.init(alloc);
        defer warned_skipping.deinit();

        var expected_types = std.ArrayListUnmanaged(wasm.ValueType){};
        defer expected_types.deinit(alloc);
        var expected = std.ArrayListUnmanaged(wasm.Value){};
        defer expected.deinit(alloc);
        var expected_error: ?anyerror = null;

        for (self.commands, 0..) |cmd, cmd_idx| {
            if (@errorReturnTrace()) |trace| {
                trace.index = 0;
                @memset(trace.instruction_addresses, undefined);
            }
            errdefer LOGGER.info("command \"{}\" ({d})", .{
                std.zig.fmtEscapes(cmd.type),
                cmd_idx,
            });
            const cmd_type = std.meta.stringToEnum(WastCommandType, cmd.type) orelse {
                if (!warned_skipping.contains(cmd.type)) {
                    LOGGER.warn("TODO: skipping unknown command \"{}\"", .{
                        std.zig.fmtEscapes(cmd.type),
                    });
                    try warned_skipping.insert(cmd.type);
                }
                continue;
            };

            defer expected_types.clearRetainingCapacity();
            defer expected.clearRetainingCapacity();
            defer expected_error = null;
            switch (cmd_type) {
                .module => {
                    const filename = cmd.filename.?;
                    maybe_module_filename = filename;
                    const file = try dir.openFile(filename, .{});
                    defer file.close();
                    module = blk: {
                        var stream = std.io.countingReader(file.reader());
                        errdefer LOGGER.info("error while decoding at offset 0x{x:0>4}", .{
                            stream.bytes_read,
                        });
                        errdefer objdump(alloc, dir, filename) catch {};
                        const mut = try arena_alloc.create(wasm.Module);
                        mut.* = try wasm.load(alloc, arena_alloc, stream.reader());
                        break :blk mut;
                    };
                    instance = try store.instantiate(import_group, module, &interpreter);
                    if (cmd.name) |name|
                        try named_instances.put(alloc, name, instance);
                    continue;
                },
                .register => {
                    try store.register(import_group, cmd.as.?, if (cmd.name) |n|
                        named_instances.get(n).?
                    else
                        instance);
                    continue;
                },
                .action => {},
                .assert_unlinkable => {
                    const module_type_text = cmd.module_type orelse {
                        LOGGER.warn("skipping unspecified module type", .{});
                        continue;
                    };
                    if (!std.mem.eql(u8, module_type_text, "binary")) {
                        LOGGER.warn("skipping unsupported module type: \"{}\"", .{
                            std.zig.fmtEscapes(module_type_text),
                        });
                        continue;
                    }

                    const filename = cmd.filename.?;
                    maybe_module_filename = filename;

                    const file = try dir.openFile(filename, .{});
                    defer file.close();
                    module = blk: {
                        const mut = try arena_alloc.create(wasm.Module);
                        mut.* = try wasm.load(alloc, arena_alloc, file.reader());
                        break :blk mut;
                    };
                    const result = store.instantiate(import_group, module, &interpreter);

                    const UNLINKABLE_TEXT_MAP = std.ComptimeStringMap(anyerror, &.{
                        .{ "unknown import", error.MissingImport },
                        .{ "incompatible import type", error.IncompatibleImport },
                    });
                    const exp = UNLINKABLE_TEXT_MAP.get(cmd.text.?) orelse {
                        LOGGER.warn("unknown error text \"{}\"", .{
                            std.zig.fmtEscapes(cmd.text.?),
                        });
                        continue;
                    };
                    try std.testing.expectError(exp, result);
                    continue;
                },
                .assert_invalid, .assert_malformed => {
                    const module_type_text = cmd.module_type orelse {
                        LOGGER.warn("skipping unspecified module type", .{});
                        continue;
                    };
                    if (!std.mem.eql(u8, module_type_text, "binary"))
                        continue;

                    errdefer LOGGER.info("expected error: \"{}\"", .{
                        std.zig.fmtEscapes(cmd.text.?),
                    });

                    const filename = cmd.filename.?;
                    maybe_module_filename = filename;

                    const file = try dir.openFile(filename, .{});
                    defer file.close();
                    const result = wasm.load(alloc, arena_alloc, file.reader());

                    const err = LOAD_ERROR_TEXT_MAP.get(cmd.text.?) orelse blk: {
                        LOGGER.warn("unknown error text \"{}\"", .{
                            std.zig.fmtEscapes(cmd.text.?),
                        });
                        break :blk error.Invalid;
                    };

                    try std.testing.expectError(err, result);
                },
                .assert_uninstantiable => {
                    const module_type_text = cmd.module_type orelse {
                        LOGGER.warn("skipping unspecified module type", .{});
                        continue;
                    };
                    if (!std.mem.eql(u8, module_type_text, "binary")) {
                        LOGGER.warn("skipping unsupported module type: \"{}\"", .{
                            std.zig.fmtEscapes(module_type_text),
                        });
                        continue;
                    }

                    const filename = cmd.filename.?;
                    maybe_module_filename = filename;

                    const file = try dir.openFile(filename, .{});
                    defer file.close();
                    module = blk: {
                        const mut = try arena_alloc.create(wasm.Module);
                        mut.* = try wasm.load(alloc, arena_alloc, file.reader());
                        break :blk mut;
                    };

                    const result = store.instantiate(import_group, module, &interpreter);

                    const err = TRAP_TEXT_MAP.get(cmd.text.?) orelse {
                        LOGGER.warn("unknown error text \"{}\"", .{
                            std.zig.fmtEscapes(cmd.text.?),
                        });
                        continue;
                    };
                    try std.testing.expectError(err, result);
                },
                .assert_return => {
                    if (cmd.expected) |exps| {
                        try expected_types.ensureUnusedCapacity(alloc, exps.len);
                        try expected.ensureUnusedCapacity(alloc, exps.len);
                        for (exps) |exp| {
                            expected_types.appendAssumeCapacity(exp.type);
                            expected.appendAssumeCapacity(try exp.toWasmValue());
                        }
                    }
                },
                .assert_trap, .assert_exhaustion => {
                    expected_error = TRAP_TEXT_MAP.get(cmd.text.?) orelse {
                        LOGGER.warn("unknown error text \"{}\"", .{
                            std.zig.fmtEscapes(cmd.text.?),
                        });
                        continue;
                    };
                },
            }

            if (cmd.action) |action| {
                try action.run(
                    alloc,
                    &store,
                    &interpreter,
                    &named_instances,
                    instance,
                    expected_types.items,
                    expected_error orelse expected.items,
                );
            }
        }
    }
};

const WastCommand = struct {
    type: []const u8,
    line: usize,
    filename: ?[]const u8 = null,
    action: ?WastAction = null,
    expected: ?[]const WastValue = null,
    text: ?[]const u8 = null,
    module_type: ?[]const u8 = null,
    as: ?[]const u8 = null,
    name: ?[]const u8 = null,
};

const WastCommandType = enum {
    module,
    register,
    action,
    assert_unlinkable,
    assert_invalid,
    assert_malformed,
    assert_uninstantiable,
    assert_return,
    assert_trap,
    assert_exhaustion,
};

const WastAction = struct {
    type: enum {
        invoke,
        get,
    },
    field: []const u8,
    module: ?[]const u8 = null,
    args: ?[]const WastValue = null,

    fn run(
        self: WastAction,
        alloc: std.mem.Allocator,
        store: *wasm.Store,
        interpreter: *wasm.Interpreter,
        instances: *const std.StringHashMapUnmanaged(*wasm.Instance),
        default_instance: *wasm.Instance,
        types: []const wasm.ValueType,
        expected: anyerror![]const wasm.Value,
    ) !void {
        // Log the invocation's symbol, if there is an error.
        errdefer LOGGER.info("export symbol \"{}\"", .{
            std.zig.fmtEscapes(self.field),
        });

        const instance = if (self.module) |mod| instances.get(mod).? else default_instance;

        // Lookup the export for the symbol.
        const exported = instance.module.lookup(self.field) orelse
            return error.ExportNotFound;

        switch (self.type) {
            .invoke => {
                defer interpreter.stack.clearRetainingCapacity();

                // Ensure the export is a function.
                const funcidx = switch (exported) {
                    .function => |idx| idx,
                    else => return error.ExportNotFunction,
                };

                // Get the concrete function instance from the module instance.
                const funcinst = instance.functions[funcidx.func_index];

                // Convert the WAST values to WASM values.
                const args = self.args.?;
                const parameters = try alloc.alloc(wasm.Value, args.len);
                defer alloc.free(parameters);
                for (parameters, args) |*dest, src|
                    dest.* = try src.toWasmValue();

                // Log the invocation's parameters once converted to the WASM representation.
                errdefer for (parameters) |param|
                    LOGGER.info("parameter {}", .{param});

                // Invoke the function.
                const result = interpreter.call(store, funcinst, parameters);

                // Expect the result.
                if (expected) |exp_values| {
                    const act_values = try result;
                    try std.testing.expectEqual(exp_values.len, act_values.len);
                    for (types, exp_values, act_values) |typ, exp, act|
                        try expectValue(typ, exp, act);
                } else |exp_err| {
                    try std.testing.expectError(exp_err, result);
                }
            },
            .get => {
                const globidx = switch (exported) {
                    .global => |v| v,
                    else => return error.ExportNotGlobal,
                };
                const exp_values = expected catch unreachable;
                std.debug.assert(exp_values.len == 1);
                const globinst = instance.globals[globidx.global_index];
                try expectValue(globinst.type.type, exp_values[0], globinst.value);
            },
        }
    }
};

fn expectValue(typ: wasm.ValueType, expected: wasm.Value, actual: wasm.Value) !void {
    switch (typ) {
        inline .f32, .f64 => |tag| {
            const exp_float = @field(expected, @tagName(tag));
            const act_float = @field(actual, @tagName(tag));
            if (std.math.isNan(exp_float)) {
                try std.testing.expect(std.math.isNan(act_float));
            } else if (std.math.isPositiveInf(exp_float)) {
                try std.testing.expect(std.math.isPositiveInf(act_float));
            } else if (std.math.isNegativeInf(exp_float)) {
                try std.testing.expect(std.math.isNegativeInf(act_float));
            } else {
                try std.testing.expectEqual(exp_float, act_float);
            }
        },
        inline else => |tag| {
            const exp_payload = @field(expected, @tagName(tag));
            const act_payload = @field(actual, @tagName(tag));
            try std.testing.expectEqual(exp_payload, act_payload);
        },
    }
}

const WastValue = struct {
    type: wasm.ValueType,
    value: []const u8 = "",

    pub fn toWasmValue(self: WastValue) !wasm.Value {
        errdefer LOGGER.info("value \"{}\"", .{std.zig.fmtEscapes(self.value)});
        return switch (self.type) {
            .i32 => .{ .i32 = try std.fmt.parseUnsigned(u32, self.value, 10) },
            .i64 => .{ .i64 = try std.fmt.parseUnsigned(u64, self.value, 10) },
            // `wast2json` writes floats as the decimal encoded integer representation of their
            // bits. This was determined from examining their source.
            .f32 => .{ .f32 = if (std.mem.eql(u8, self.value, "nan:canonical"))
                std.math.nan(f32)
            else if (std.mem.eql(u8, self.value, "nan:arithmetic"))
                std.math.nan(f32)
            else
                @bitCast(try std.fmt.parseUnsigned(u32, self.value, 10)) },
            .f64 => .{ .f64 = if (std.mem.startsWith(u8, self.value, "nan:canonical"))
                std.math.nan(f64)
            else if (std.mem.eql(u8, self.value, "nan:arithmetic"))
                std.math.nan(f64)
            else
                @bitCast(try std.fmt.parseUnsigned(u64, self.value, 10)) },
            .externref => .{
                .externref = if (std.mem.eql(u8, "null", self.value))
                    null
                else
                    @constCast(@ptrCast(&.{})),
            },
            .funcref => .{
                .funcref = if (std.mem.eql(u8, "null", self.value)) null else unreachable,
            },
            inline else => |tag| std.debug.panic("TODO: implement value type {s}", .{@tagName(tag)}),
        };
    }
};

const spectest = struct {
    pub fn print(_: *anyopaque, _: *wasm.Stack) !void {}
    pub fn print_i32(_: *anyopaque, stack: *wasm.Stack) !void {
        _ = stack.parameter().i32;
    }
    pub fn print_i64(_: *anyopaque, stack: *wasm.Stack) !void {
        _ = stack.parameter().i64;
    }
    pub fn print_f32(_: *anyopaque, stack: *wasm.Stack) !void {
        _ = stack.parameter().f32;
    }
    pub fn print_f64(_: *anyopaque, stack: *wasm.Stack) !void {
        _ = stack.parameter().f64;
    }
    pub fn print_i32_f32(_: *anyopaque, stack: *wasm.Stack) !void {
        _ = stack.parameter().f32;
        _ = stack.parameter().i32;
    }
    pub fn print_f64_f64(_: *anyopaque, stack: *wasm.Stack) !void {
        _ = stack.parameter().f64;
        _ = stack.parameter().f64;
    }
};

fn objdump(alloc: std.mem.Allocator, dir: std.fs.Dir, path: []const u8) !void {
    var proc = std.ChildProcess.init(&.{ "wasm-objdump", "-x", "-d", "-s", path }, alloc);
    proc.cwd_dir = dir;
    _ = try proc.spawnAndWait();
}
