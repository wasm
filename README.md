# wasm

A wasm interpretter.

## Attribution

- Most of the files ending with ".wast" in the [wast/](./wast) directory are copied from the WASM
  specification's test directory. They are licensed under the Apache License 2.0. For more
  information please refer to their
  [LICENSE](https://github.com/WebAssembly/spec/blob/3725d1a51ece93c22b353c2a6fbdb7466e25c215/LICENSE).
