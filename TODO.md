# TODO

Project level TODO items which should be addressed at some point.

- Documentation.
- Rename types to be more consistent with the specification. For instance, `Table` should become
  `TableType`.
- Determine an appropriate policy for where to place type definitions which are shared among files.
  Right now, I'm thinking the location in which the type is first instantiated should be where it is
  defined. Everything else imports from it. A competing policy would be a dedicated file for common
  types.
- Implement initialization of tables and memories in `instantiate` using `Interpreter.call`, calling
  a virtual function.
- Unify pushing the first frame of `Interpreter.call` with the execution of the `call` and
  `call_indirect` instructions.
