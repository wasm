const std = @import("std");

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const mod = b.addModule("wasm", .{
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    // Build a static library for using the interpetter.
    const lib = b.addStaticLibrary(.{
        .name = "wasm",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    b.installArtifact(lib);

    // Run an executable for driving WAST tests.
    var wast = b.addExecutable(.{
        .name = "wast",
        .root_source_file = .{ .path = "wast/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    wast.root_module.addImport("wasm", mod);

    // Create a step for running the tests.
    const test_step = b.step("test", "Run unit tests");

    // Run unit tests for the library.
    const lib_unit_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    const run_lib_unit_tests = b.addRunArtifact(lib_unit_tests);
    test_step.dependOn(&run_lib_unit_tests.step);

    // Run WAST tests.
    var wast_filter = std.BufSet.init(b.allocator);
    defer wast_filter.deinit();
    var run_all_wast_tests = false;
    if (b.args) |args| {
        for (args) |arg| {
            if (std.mem.eql(u8, "--all", arg)) {
                run_all_wast_tests = true;
            }
            try wast_filter.insert(arg);
        }
    } else {
        // TODO: Remove this block when all WAST tests first pass.
        const allow_list = try b.build_root.handle.readFileAlloc(
            b.allocator,
            "allowed-wast-tests.txt",
            std.math.maxInt(usize),
        );
        defer b.allocator.free(allow_list);

        var iter = std.mem.tokenize(u8, allow_list, &std.ascii.whitespace);
        while (iter.next()) |item|
            try wast_filter.insert(item);
    }
    const wast_dir = try b.build_root.handle.openDir("wast", .{ .iterate = true });
    var wast_dir_iter = wast_dir.iterate();
    while (try wast_dir_iter.next()) |entry| {
        if (entry.kind != .file or !std.mem.endsWith(u8, entry.name, ".wast"))
            continue;
        const stem = std.fs.path.stem(entry.name);
        if (!run_all_wast_tests and wast_filter.count() > 0 and !wast_filter.contains(stem))
            continue;

        const wast_source = .{ .path = b.pathJoin(&.{ "wast", entry.name }) };
        var generate_wast_json = b.addSystemCommand(&.{"wast2json"});
        generate_wast_json.addFileArg(wast_source);
        generate_wast_json.addArg("--output");
        const output = generate_wast_json.addOutputFileArg(b.fmt("{s}.json", .{stem}));

        const run_wast_test = b.addRunArtifact(wast);
        run_wast_test.addFileArg(wast_source);
        run_wast_test.addFileArg(output);
        run_wast_test.setName(b.fmt("run wast test \"{}\"", .{std.zig.fmtEscapes(stem)}));
        test_step.dependOn(&run_wast_test.step);
    }

    const fuzz_step = b.step("fuzz", "Fuzz test the library.");
    const fuzzer_step = b.step("fuzzer", "Run the fuzzer.");
    const fuzzer = b.addExecutable(.{
        .name = "wasm-fuzzer",
        .root_source_file = .{ .path = "fuzz.zig" },
        .target = target,
        .optimize = optimize,
    });
    fuzzer.root_module.addImport("wasm", mod);
    const run_fuzzer = b.addRunArtifact(fuzzer);
    if (b.args) |args|
        run_fuzzer.addArgs(args);
    fuzzer_step.dependOn(&run_fuzzer.step);
    const run_fuzz_test = b.addSystemCommand(&.{
        "honggfuzz",
        "--stdin_input",
        "--noinst",
        "--workspace",
        "fuzz-out",
        "--input",
    });
    run_fuzz_test.addDirectoryArg(.{ .path = "fuzzdata" });
    if (b.args) |args|
        run_fuzz_test.addArgs(args);
    run_fuzz_test.addArg("--");
    run_fuzz_test.addFileArg(fuzzer.getEmittedBin());
    fuzz_step.dependOn(&run_fuzz_test.step);

    // Add a step to generate test WASM binaries. These are to be checked into the respository for
    // easier use in tests.
    const generate_tests = b.step("generate-tests", "Generate WASM test binaries from source.");
    const tests_dir = try b.build_root.handle.openDir("src/tests", .{ .iterate = true });
    var tests_dir_iter = tests_dir.iterate();
    const test_wasm_target = b.resolveTargetQuery(.{
        .cpu_arch = .wasm32,
        .cpu_model = .baseline,
        .os_tag = .freestanding,
        .abi = .none,
    });
    var copy_test_wasms_to_source = b.addWriteFiles();
    while (try tests_dir_iter.next()) |entry| {
        if (entry.kind != .file or !std.mem.endsWith(u8, entry.name, ".zig"))
            continue;
        const stem = std.fs.path.stem(entry.name);
        const test_wasm_exe = b.addExecutable(.{
            .name = stem,
            .root_source_file = .{ .path = b.pathJoin(&.{ "src", "tests", entry.name }) },
            .target = test_wasm_target,
            .optimize = .ReleaseSmall,
            .strip = true,
        });
        test_wasm_exe.entry = .disabled;
        test_wasm_exe.root_module.export_symbol_names = &.{"run"};
        test_wasm_exe.want_lto = true;

        generate_tests.dependOn(&test_wasm_exe.step);
        copy_test_wasms_to_source.addCopyFileToSource(
            test_wasm_exe.getEmittedBin(),
            b.pathJoin(&.{ "src", "tests", b.fmt("{s}.wasm", .{stem}) }),
        );
    }
    generate_tests.dependOn(&copy_test_wasms_to_source.step);
}
