const std = @import("std");
const wasm = @import("wasm");

const LOGGER = std.log.scoped(.fuzz);

pub const wasm_options = struct {
    pub fn trace(event: wasm.TraceEvent) void {
        LOGGER.debug("trace {}", .{event});
    }
};

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{ .enable_memory_limit = true }){
        .requested_memory_limit = 64 * 1 << 20,
    };
    defer std.debug.assert(gpa.deinit() == .ok);
    const alloc = gpa.allocator();

    var args = try std.process.argsWithAllocator(alloc);
    defer args.deinit();
    std.debug.assert(args.skip());

    const stdin_file = std.io.getStdIn();
    var stdin_buffered = std.io.bufferedReader(stdin_file.reader());
    const stdin = stdin_buffered.reader();

    var arena = std.heap.ArenaAllocator.init(alloc);
    defer arena.deinit();
    const arena_alloc = arena.allocator();

    var interpreter = wasm.Interpreter.init(alloc);
    defer interpreter.deinit();

    var store = wasm.Store.init(alloc);
    defer store.deinit();
    const import_group = store.importGroup();

    const module = try wasm.load(alloc, arena_alloc, stdin);
    const instance = try store.instantiate(import_group, &module, &interpreter);
    _ = instance;

    std.process.cleanExit();
}
